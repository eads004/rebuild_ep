rm temp/*
for f in /home/spenteco/0/rebuild_ep_data/blacklab/temp/*
do
    file_name=`basename "$f"`
    echo $f, $file_name
    java -jar /home/spenteco/0/saxon9he.jar $f prep/defaultsource.xsl > /home/spenteco/0/rebuild_ep_data/blacklab/temp2/$file_name
done
java -Xmx12G -Xms800m -cp /home/spenteco/3/blacklab_etc/BlackLab-2.2.0/core/target/blacklab-2.2.0.jar nl.inl.blacklab.tools.IndexTool create eebotcp /home/spenteco/0/rebuild_ep_data/blacklab/temp2/ eebotcp
