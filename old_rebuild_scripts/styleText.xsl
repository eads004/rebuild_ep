<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:strip-space elements="tei:app tei:choice tei:persName tei:possibleChoice tei:newChoice"/> 
    <!--<xsl:strip-space elements="tei:app tei:choice tei:persName tei:possibleChoice tei:newChoice tei:lem tei:rdg"/>-->
    
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="openparen">\(</xsl:variable>
    <xsl:variable name="closeparen">\)</xsl:variable>
    
    <xsl:output 
        method="xhtml" 
        encoding="utf-8" 
        omit-xml-declaration="yes"
        indent="no"/>
        
    <!-- DROP THESE TAGS ********************************************** -->

    <xsl:template match="tei:fw"/>
    <xsl:template match="tei:pc"/>
    <xsl:template match="tei:figure[@rend='do_not_display']"/>
        
    <!-- DISAMBIGUATION GLOSSES ********************************************** -->

    <xsl:template match="tei:note[@type='delete']"/>

    <xsl:template match="tei:note[@type='disamb']">
        <span>
            <xsl:attribute name="class">disamb_note</xsl:attribute>
            <xsl:attribute name="disamb_type"><xsl:value-of select="@disamb_type"/></xsl:attribute>
            <xsl:attribute name="line_number"><xsl:value-of select="@line_number"/></xsl:attribute>
            <xsl:attribute name="ref"><xsl:value-of select="@ref"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:term">
        <span>
            <xsl:attribute name="class">lemmaNoPad</xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
        
    <!-- "SPECIAL' T TAG ********************************************** -->

    <xsl:template match="tei:t"><t><xsl:apply-templates/></t></xsl:template>
        
    <!-- HANDLE SPECIFIC TAGS ***************************************** -->

    <xsl:template match="tei:div">
        <div>
            <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(@type, ' ', replace(replace(replace(@place, '\.', '_'), $openparen, '_'), $closeparen, '_'), ' ', replace(replace(replace(replace(@rend, '\.', '_'), $openparen, '_'), $closeparen, '_'), '-', '_')))"/></xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:titlePage|tei:docTitle|tei:titlePart|tei:byline|tei:docImprint|tei:lg|tei:p|tei:closer|tei:signed|tei:dateline|tei:seg|tei:argument|tei:pubPlace|tei:note|tei:salute|tei:castList|tei:castItem|tei:persName|tei:speaker|tei:trailer">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(local-name(.), ' ', @class, ' ', @type, ' ', replace(replace(replace(@place, '\.', '_'), $openparen, '_'), $closeparen, '_'), ' ', replace(replace(replace(replace(@rend, '\.', '_'), $openparen, '_'), $closeparen, '_'), '-', '_')))"/></xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:head">
        <xsl:choose>
            <xsl:when test="contains(@type, 'onlineRunningTitle')"/>
            <xsl:when test="contains(@type, 'headnote')">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <span>
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(local-name(.), ' ', @class, ' ', @type, ' ', replace(replace(replace(@place, '\.', '_'), $openparen, '_'), $closeparen, '_'), ' ', replace(replace(replace(replace(@rend, '\.', '_'), $openparen, '_'), $closeparen, '_'), '-', '_')))"/></xsl:attribute>
                    <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="contains(@ed, 'first_edition')">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <span>
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(local-name(.), ' ', @class, ' ', @type, ' ', replace(replace(replace(@place, '\.', '_'), $openparen, '_'), $closeparen, '_'), ' ', replace(replace(replace(replace(@rend, '\.', '_'), $openparen, '_'), $closeparen, '_'), '-', '_')))"/></xsl:attribute>
                    <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- SC WOODCUTS *************************************************** -->

    <xsl:template match="tei:sp">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="concat('sp', ' ', @rend)"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:figDesc">
        <span>
            <xsl:attribute name="class">figDesc</xsl:attribute>
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <xsl:template match="tei:graphic">
        <xsl:if test="@url">
            <img>
                <xsl:attribute name="class">textIllustration</xsl:attribute>
                <xsl:attribute name="src"><xsl:value-of select="concat('../', @url)"/></xsl:attribute>
            </img>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:g">
        <xsl:if test="@ref='#que'">
            <xsl:text>q</xsl:text>
            <span>
                <xsl:attribute name="class">latinExpansion</xsl:attribute>
                <xsl:text>ue</xsl:text>
            </span>
        </xsl:if>
        <xsl:if test="@ref='#quam'">
            <xsl:text>q</xsl:text>
            <span>
                <xsl:attribute name="class">latinExpansion</xsl:attribute>
                <xsl:text>uam</xsl:text>
            </span>
        </xsl:if>
    </xsl:template>

    <!-- SIGNATURES **************************************************** -->

    <!--
    <xsl:template match="tei:milestone">
        <xsl:if test="@unit='sigForDisplay'">
            <span>
                <xsl:attribute name="class"><xsl:value-of select="concat('signatureForDisplay', ' ', @class)"/></xsl:attribute>
                <xsl:attribute name="rend"><xsl:value-of select="@rend"/></xsl:attribute>
                <xsl:attribute name="n"><xsl:value-of select="@n"/></xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:attribute name="witness"><xsl:value-of select="@corresp"/></xsl:attribute>
            </span>
        </xsl:if>
    </xsl:template>
    -->
    <xsl:template match="tei:pb|pb">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="concat('signatureForDisplay', ' ', @class)"/></xsl:attribute>
            <xsl:attribute name="rend"><xsl:value-of select="@rend"/></xsl:attribute>
            <xsl:attribute name="n"><xsl:value-of select="@n"/></xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="@signaturesAnchor"/></xsl:attribute>
            <xsl:attribute name="witness"><xsl:value-of select="@corresp"/></xsl:attribute>
        </span>
    </xsl:template>

    <!-- THE LIST IN THEATRE **************************************************** -->

    <xsl:template match="tei:item">
            <span>
                <xsl:attribute name="class">item</xsl:attribute>
                <xsl:apply-templates/>
            </span>
    </xsl:template>
    
    <!-- SPAN (LINE NUMBERS) ****************************************** -->

    <xsl:template match="tei:span">
        <xsl:if test="@class='disambAnchor'">
            <span>
                <xsl:attribute name="class">glossesAnchor</xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@class='glossLineNumber'">
            <span>
                <xsl:attribute name="class">glossLineNumber</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='stanzaNumber'">
            <span>
                <xsl:attribute name="class">stanzaNumber</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='lineNumber'">
            <span>
                <xsl:attribute name="class"><xsl:value-of select="concat('lineNumber', ' ', @class)"/></xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='hiddenLineNumber'">
            <span>
                <xsl:attribute name="class"><xsl:value-of select="concat('hiddenLineNumber', ' ', @class)"/></xsl:attribute>
                <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='lineNumberPart2'">
            <span>
                <xsl:attribute name="class">lineNumberPart2</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='for_testing_5'">
                <xsl:apply-templates/>
        </xsl:if>
        <xsl:if test="@type='completeLineNumber'">
            <span>
                <xsl:attribute name="class">completeLineNumber</xsl:attribute>
                <xsl:apply-templates/>
            </span>
            <!--<a><xsl:attribute name="name"><xsl:apply-templates/></xsl:attribute></a>-->
        </xsl:if>
        <xsl:if test="@type='headingLineWithLineNumber'">
            <span>
                <xsl:attribute name="class">headingLineWithLineNumber</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='indentAndItalicize'">
            <span>
                <xsl:attribute name="class">indentAndItalicize</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='float_right'">
            <span>
                <xsl:attribute name="class">float_right</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@class='apparatusLineNumber'">
            <span>
                <xsl:attribute name="class">apparatusLineNumber</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
    </xsl:template>

    <xsl:template match="span">
        <xsl:if test="@class='showOriginalValue_showSilentModernization'">
            <span>
                <xsl:attribute name="class">showOriginalValue_showSilentModernization</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@class='showOriginalValue'">
            <span>
                <xsl:attribute name="class">showOriginalValue</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
        <xsl:if test="@type='for_testing_5'">
                <xsl:apply-templates/>
        </xsl:if>
        <xsl:if test="@class='showOriginalValue'">
            <span>
                <xsl:attribute name="class">showOriginalValue</xsl:attribute>
                <xsl:apply-templates/>
            </span>
        </xsl:if>
    </xsl:template>

    <!-- LB AND L ***************************************************** -->

    <xsl:template match="tei:l">
        <a><xsl:attribute name="name"><xsl:value-of select="@n"/></xsl:attribute></a>
        <span>
            <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat(local-name(.), ' ', @type, ' ', replace(replace(replace(@place, '\.', '_'), $openparen, '_'), $closeparen, '_'), ' ', replace(replace(replace(replace(@rend, '\.', '_'), $openparen, '_'), $closeparen, '_'), '-', '_')))"/></xsl:attribute>
            <xsl:attribute name="lineNumber"><xsl:value-of select="@n"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:lb">

        <xsl:choose>
            <xsl:when test="contains(@break, 'no')"/>
            <xsl:otherwise>
                <a><xsl:attribute name="name"><xsl:value-of select="@n"/></xsl:attribute></a>
                <xsl:choose>
                    <xsl:when test="contains(@ed, 'webArchive')">
                        <span>
                            <xsl:attribute name="class">displayLineBreak</xsl:attribute>
                            <xsl:attribute name="lineNumber"><xsl:value-of select="@n"/></xsl:attribute>
                        </span>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="not(ancestor::tei:p)">
                                <xsl:choose>
                                    <xsl:when test="not(ancestor::tei:closer)">
                                        <span>
                                            <xsl:attribute name="class">displayLineBreakNotWeb</xsl:attribute>
                                        </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span>
                                            <xsl:attribute name="class">displayLineBreak</xsl:attribute>
                                        </span>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- CHOICE-ORIG-REG AND APP-RDG-LEM  ***************************** -->

    <!--
    <xsl:template match="tei:newChoice">
        <xsl:choose>
            <xsl:when test="@class='glossesAnchor'">
                <span><xsl:attribute name="class"><xsl:value-of select="concat('gloss ',@gloss_target,' ',translate(@n,'.','_'), ' ', @class, ' ', @choice_type)"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="concat('Anchor', translate(@n,'.','_'))"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:when>
            <xsl:otherwise>
                <span><xsl:attribute name="class"><xsl:value-of select="concat(@gloss_target,' ',@choice_type)"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:possibleChoice">
        <span><xsl:attribute name="class"><xsl:value-of select="@choice_type"/></xsl:attribute><xsl:apply-templates/></span>
    </xsl:template>
    -->

    <xsl:template match="tei:orig">
        <xsl:choose>
            <xsl:when test="@class='glossesAnchor'">
                <span><xsl:attribute name="class"><xsl:value-of select="concat('orig ','gloss ',@gloss_target,' ',translate(@n,'.','_'), ' ', @class, ' ', @choice_type)"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="concat('Anchor', translate(@n,'.','_'))"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:when>
            <xsl:otherwise>
                <span><xsl:attribute name="class"><xsl:value-of select="concat('orig ',@gloss_target,' ',@choice_type)"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:reg">
        <xsl:choose>
            <xsl:when test="@class='glossesAnchor'">
                <span><xsl:attribute name="class"><xsl:value-of select="concat('reg ','gloss ',@gloss_target,' ',translate(@n,'.','_'), ' ', @class, ' ', @choice_type)"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="concat('Anchor', translate(@n,'.','_'))"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:when>
            <xsl:otherwise>
                <span><xsl:attribute name="class"><xsl:value-of select="concat('reg ',@gloss_target,' ',@choice_type)"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--<xsl:template match="tei:choice"><xsl:apply-templates/></xsl:template>-->

    <xsl:template match="tei:choice">
        <xsl:choose>
            <xsl:when test="@class='glossesAnchor'">
                <span><xsl:attribute name="class"><xsl:value-of select="concat('gloss ',@gloss_target,' ',translate(@n,'.','_'), ' ', @class, ' ', @choice_type)"/></xsl:attribute><xsl:attribute name="id"><xsl:value-of select="concat('Anchor', translate(@n,'.','_'))"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:when>
            <xsl:otherwise>
                <span><xsl:attribute name="class"><xsl:value-of select="concat(@gloss_target,' ',@choice_type)"/></xsl:attribute><xsl:apply-templates/></span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:lem">
        <span>
            <xsl:attribute name="varSeq"><xsl:value-of select="@varSeq"/></xsl:attribute>
            <xsl:choose>
                <xsl:when test="@eclecticCopyText='yes'">
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat('lem ',@wit,' ect_on'))"/></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat('lem ',@wit,' ect_off'))"/></xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:rdg">
        <span>
            <xsl:attribute name="varSeq"><xsl:value-of select="@varSeq"/></xsl:attribute>
            <xsl:choose>
                <xsl:when test="@eclecticCopyText='yes'">
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat('rdg ',@wit,' ect_on'))"/></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat('rdg ',@wit,' ect_off'))"/></xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:app">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="normalize-space(concat('app ', @type, ' ', @class))"/></xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <xsl:attribute name="appN"><xsl:value-of select="@appN"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- REF TAGS (LINKS TO COMMENTARY ******************************** -->

    <!--<xsl:template match="tei:anchor">
        <span>
            <xsl:attribute name="class"><xsl:value-of select="concat('commentLink ',translate(@xml:id,'.','_'))"/></xsl:attribute>
            <t><span class="headnote_literal">[headnote]</span></t>
        </span>
    </xsl:template>-->

    <xsl:template match="tei:ref">
        <xsl:choose>

            <xsl:when test="@type='commentary'">
                <xsl:choose>
                    <xsl:when test="*">
                        <span>
                            <xsl:attribute name="class"><xsl:value-of select="concat('commentLink ',translate(@n,'.','_'))"/></xsl:attribute>
                            <xsl:apply-templates/>
                        </span>
                    </xsl:when>
                    <xsl:otherwise>
                        <span>
                            <xsl:attribute name="class"><xsl:value-of select="concat('commentLink ',translate(@n,'.','_'))"/></xsl:attribute>
                            <t><span class="headnote_literal">[headnote]</span></t>
                        </span>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>

            <xsl:when test="@type='gloss'">
                <span>
                    <xsl:attribute name="class"><xsl:value-of select="concat('gloss ' ,translate(@n,'.','_'), ' ', @class)"/></xsl:attribute>
                    <xsl:attribute name="id"><xsl:value-of select="concat('Anchor', translate(@n,'.','_'))"/></xsl:attribute>
                    <xsl:choose>
                        
                        <xsl:when test="descendant::tei:lb[@ed='webArchive']">
                          <xsl:attribute name="glossLineNumber"><xsl:value-of select="descendant::tei:lb[@ed='webArchive']/@n"/></xsl:attribute>
                          <xsl:apply-templates/>
                        </xsl:when>
                        
                        <xsl:otherwise>
                            
                            <xsl:choose>
                                
                                <xsl:when test="ancestor::tei:l">
                                  <xsl:attribute name="glossLineNumber"><xsl:value-of select="ancestor::tei:l/@n"/></xsl:attribute>
                                  <xsl:apply-templates/>
                                </xsl:when>
                                
                                <xsl:when test="following::tei:lb[contains(@ed, 'webArchive')]">
                                  <xsl:attribute name="glossLineNumber"><xsl:value-of select="following::tei:lb[contains(@ed, 'webArchive')][1]/@n"/></xsl:attribute>
                                  <xsl:apply-templates/>
                                </xsl:when>
                        
                                <xsl:otherwise>
                                  <xsl:apply-templates/>
                                </xsl:otherwise>
                                
                            </xsl:choose>
                            
                        </xsl:otherwise>
                        
                    </xsl:choose>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- ************************************************************** -->

    <xsl:template match="textualApparatus">
        <span>
            <xsl:attribute name="class">textualApparatus</xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="textualNote">
        <span>
            <xsl:attribute name="class">textualNote</xsl:attribute>
            <xsl:attribute name="sig"><xsl:value-of select="@sig"/></xsl:attribute>
            <xsl:attribute name="lineNumber"><xsl:value-of select="@lineNumber"/></xsl:attribute>
            <xsl:attribute name="rend"><xsl:value-of select="@rend"/></xsl:attribute>
            <xsl:attribute name="appN"><xsl:value-of select="@appN"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- ************************************************************** -->

    <!--<xsl:template match="tei:text">
        <span>
            <xsl:attribute name="class">text</xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>-->

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>
