#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, re, glob
from lxml import etree

def extract_metadata(folderName):

    global numberOfFiles

    for inputFileName in glob.glob(folderName + '*.xml'):

        tree = etree.parse(inputFileName)                
        
        titles = tree.xpath('//tei:title', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        authors = tree.xpath('//tei:author', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        pubplaces = tree.xpath('//tei:pubPlace', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        publishers = tree.xpath('//tei:publisher', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
        dates = tree.xpath('//tei:date', 
                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
                    
        #outputString = inputFileName.split('/')[-1].split('.')[0].replace('_sourcemeta', '')
        outputString = inputFileName.split('/')[-1].split('.')[0].replace('_header', '')
        
        s = ''
        for t in dates:
            
            when = t.get('when')
            notBefore = t.get('notBefore')
            notAfter = t.get('notAfter')
            
            if when != None:
                if s > '':
                    s = s + '|'
                s = s + when
            elif notBefore != None and notAfter != None:
                notBefore = int(notBefore)
                notAfter = int(notAfter)
                if s > '':
                    s = s + '|'
                s = s + str(int((notBefore + notAfter) / 2))
                
        if s == '':
            s = ' '

        outputString = outputString + '\t' + s
        
        s = ''
        for t in authors:
            if s > '':
                s = s + '|'
            s = s + t.text
        if s == '':
            s = ' '

        outputString = outputString + '\t' + s
        
        s = ''
        for t in titles:
            if s > '':
                s = s + '|'
            s = s + t.text
        if s == '':
            s = ' '

        outputString = outputString + '\t' + s

        print(outputString)

# ----------------------------------------------------------------------
#   MAIN
#
# ----------------------------------------------------------------------

extract_metadata(sys.argv[1])
