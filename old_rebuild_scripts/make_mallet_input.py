#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, random
from lxml import etree
from nltk.corpus import stopwords

def is_number_etc(w):

    result = False

    if '.' in w:
        result = True
    elif re.sub('[0-9]', '', w) != w:
        result = True 
    elif re.sub('[ivx]', '', w.lower()) == '':
        result = True 

    return result

sw = set(stopwords.words('english') + ['thee', 'thy', 'thou', 'thine'] + ['a','ab','ac','ad','at','atque','aut','autem','cum','de','dum','e','erant','erat','est','et','etiam','ex','haec','hic','hoc','in','ita','me','nec','neque','non','per','qua','quae','quam','qui','quibus','quidem','quo','quod','re','rebus','rem','res','sed','si','sic','sunt','tamen','tandem','te','ut','vel'])

bogus_letters = set([u'•', u'…', u'〈', u'〉'])

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
#MORPHADORNER_FOLDER = 'test_adorned/'

MALLET_INPUT_FILE_NAME = '/home/data/all_3_prototype_rebuild/all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'

output_file = codecs.open(MALLET_INPUT_FILE_NAME, 'w', encoding='utf-8')

for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

    # BAD XML FROM MORPHADORNER
    if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

        print 'processing', f.split('/')[-1].split('.')[0]

        tokens = [f.split('/')[-1].split('.')[0], 'en']

        tree = etree.parse(f)

        for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
            if w.get('lem') != None and w.get('lem') > '' and w.get('lem') not in sw and '|' not in w.get('lem') and is_number_etc(w.get('lem')) == False:
                is_bogus = False
                for c in bogus_letters:
                    if c in w.get('lem'):
                        is_bogus = True
                if is_bogus == False:
                    tokens.append(w.get('lem').lower())

        if len(tokens) > 2:
            output_file.write(' '.join(tokens) + '\n')

output_file.close()
