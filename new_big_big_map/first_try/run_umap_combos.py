#!/usr/bin/env python

from gensim import corpora, models, matutils
import umap
from sklearn.preprocessing import StandardScaler
import json

DATA_FOLDER = '/home/spenteco/0/rebuild_ep_data/lsa_umap/'

lsi_model = models.LsiModel.load(DATA_FOLDER + 'all_eebo.lsa_umap.text.lsa_model')

corpus_lsi = corpora.MmCorpus(DATA_FOLDER + 'all_eebo.lsa_umap.text.lsa')

matrix = matutils.corpus2dense(corpus_lsi, lsi_model.num_topics)
matrix = matrix.T

f = open(DATA_FOLDER + 'expanded_label_data.json', 'r', encoding='utf-8')
labels = json.load(f)
f.close()


for n_neighbors in (5, 20, 50, 100, 200):
    
    for min_dist in (0.0, 0.25, 0.5, 0.75, 0.99):
        
        for metric in ['euclidean',
                        'manhattan',
                        'chebyshev',
                        'minkowski',
                        'canberra',
                        'braycurtis',
                        'mahalanobis',
                        'wminkowski',
                        'seuclidean',
                        'cosine',
                        'correlation',]:

            result_file = 'UMAP.n_neighbors_' + str(n_neighbors) + \
                        '_min_dist_' + str(min_dist) + \
                        '_metric_' + str(metric) + \
                        '.json'

            try:
            #if True:
            
                reducer = umap.UMAP(n_neighbors=n_neighbors, min_dist=min_dist, metric=metric)

                scaled_matrix = StandardScaler().fit_transform(matrix)

                embedding = reducer.fit_transform(scaled_matrix)
                
                result = []
                
                for a in range(0, len(embedding)):
                
                    row = labels[a]
                    row['x'] = float(embedding[a][0])
                    row['y'] = float(embedding[a][1])
                
                    result.append(row)
    
                f = open(DATA_FOLDER + result_file, 'w', encoding='utf-8')
                f.write(json.dumps(result, indent=4))
                f.close()
                
                print('OK', result_file)
                
            except:
                
                print('ERROR', result_file)
                
                
                
