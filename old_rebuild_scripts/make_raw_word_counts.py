#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, math
from lxml import etree
from collections import Counter

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
#MORPHADORNER_FOLDER = 'test_adorned/'

LABELS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.WORD_COUNTS.' + LIKENESS_DEPTH + '.labels.json'
WORD_COUNTS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.WORD_COUNTS.' + LIKENESS_DEPTH + '.raw_word_counts'

#LABELS_FILE_NAME = 'test_outputs/all_eebo.WORD_COUNTS.' + LIKENESS_DEPTH + '.labels.json'
#WORD_COUNTS_FILE_NAME = 'test_outputs/all_eebo.WORD_COUNTS.' + LIKENESS_DEPTH + '.raw_WORD_COUNTS'

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            print 'loading', file_name

            tokens = []

            tree = etree.parse(MORPHADORNER_FOLDER + file_name)

            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if w.get('lem') != None and w.get('lem') > '':

                    l = w.get('lem').lower()
                    if l == re.sub('[^a-z]', '', l):
                        tokens.append(w.get('lem').lower())

            yield tokens
        
# ----------------------------------------------------------------------

def get_labels():

    file_name_labels = []

    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

        # BAD XML FROM MORPHADORNER
        if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                    'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                    'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                    'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                    'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                    'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

            file_name_labels.append(f.split('/')[-1])

    f = codecs.open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(file_name_labels))
    f.close()

    print 'get_labels', 'len(file_name_labels)', len(file_name_labels)

def load_labels_json():

    file_name_labels = json.loads(codecs.open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    print 'load_labels_json', 'len(file_name_labels)', len(file_name_labels)

    return file_name_labels

def make_WORD_COUNTS():
    
    print 'make_WORD_COUNTS', 'STARTED'

    file_name_labels = load_labels_json()

    texts = Morphadorned_Corpus(file_name_labels)

    A_text_word_counts = []   

    for text in texts:

        A_text_word_counts.append(Counter(text))

    f = codecs.open(WORD_COUNTS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(A_text_word_counts))
    f.close()

    print 'make_WORD_COUNTS', 'len(A_text_word_counts)', len(A_text_word_counts)

if __name__ == "__main__":

    get_labels()
    make_WORD_COUNTS()
