#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, glob, json, re
from lxml import etree
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile
from nltk.corpus import stopwords

sw = set(stopwords.words('english') + ['thee', 'thy', 'thou', 'thine'] + ['a','ab','ac','ad','at','atque','aut','autem','cum','de','dum','e','erant','erat','est','et','etiam','ex','haec','hic','hoc','in','ita','me','nec','neque','non','per','qua','quae','quam','qui','quibus','quidem','quo','quod','re','rebus','rem','res','sed','si','sic','sunt','tamen','tandem','te','ut','vel'])

bogus_letters = set([u'•', u'…', u'〈', u'〉'])

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = sys.argv[1]

LABELS_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.labels.json'
DICTIONARY_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.dict'
MM_CORPUS_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.mm'
TFIDF_CORPUS_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.tfidf'
INDEX_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.index'
TMP_FILE_NAME = sys.argv[2] + 'all_eebo.tfidf.' + LIKENESS_DEPTH + '.TMP.index'

def is_number_etc(w):

    result = False

    if '.' in w:
        result = True
    elif re.sub('[0-9]', '', w) != w:
        result = True 
    elif re.sub('[ivx]', '', w.lower()) == '':
        result = True 
    elif w == '{non-roman}':
        result = True 

    return result

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            tokens = []

            tree = etree.parse(file_name[1])

            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if w.get('lemma') != None and w.get('lemma') > '' and w.get('lemma') not in sw and '|' not in w.get('lemma') and is_number_etc(w.get('lemma')) == False:
                    is_bogus = False
                    for c in bogus_letters:
                        if c in w.get('lemma'):
                            is_bogus = True
                    if is_bogus == False:
                        tokens.append(w.get('lemma').lower())

            yield tokens
        
# ----------------------------------------------------------------------

def get_labels():

    file_name_labels = []
    
    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '**/*.xml', recursive=True)):
        file_name_labels.append([f.split('/')[-1], f])

    f = open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(file_name_labels))
    f.close()

    print('get_labels', 'len(file_name_labels)', len(file_name_labels))

def load_labels_json():

    file_name_labels = json.loads(open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    print('load_labels_json', 'len(file_name_labels)', len(file_name_labels))

    return file_name_labels

def make_dictionary():
    
    print('make_dictionary', 'STARTED')

    file_name_labels = load_labels_json()

    texts = Morphadorned_Corpus(file_name_labels)
    
    my_dictionary = corpora.Dictionary(texts)

    my_dictionary.save(DICTIONARY_FILE_NAME)
    
    print('make_dictionary', 'DONE')

def make_tfidf():
    
    print('make_tfidf', 'STARTED')

    file_name_labels = load_labels_json()

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    texts = Morphadorned_Corpus(file_name_labels)

    mm_corpus = [my_dictionary.doc2bow(text) for text in texts]

    corpora.MmCorpus.serialize(MM_CORPUS_FILE_NAME, mm_corpus)

    tfidf = models.TfidfModel(mm_corpus)

    corpus_tfidf = tfidf[mm_corpus]

    corpora.MmCorpus.serialize(TFIDF_CORPUS_FILE_NAME, corpus_tfidf)
    
    print('make_tfidf', 'DONE')

def make_index():
    
    print('make_index', 'STARTED')

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    corpus_tfidf = corpora.MmCorpus(TFIDF_CORPUS_FILE_NAME)

    index_tmpfile = get_tmpfile(TMP_FILE_NAME)

    index = similarities.Similarity(index_tmpfile, corpus_tfidf, num_features=len(my_dictionary))

    index.save(INDEX_FILE_NAME)
    
    print('make_index', 'DONE')

if __name__ == "__main__":
    
    if sys.argv[3] == 'all':

        get_labels()
        make_dictionary()
        make_tfidf()
        make_index()

    elif sys.argv[3] == 'labels':
        get_labels()

    elif sys.argv[3] == 'dictionary':
        make_dictionary()

    elif sys.argv[3] == 'tfidf':
        make_tfidf()

    elif sys.argv[3] == 'index':
        make_index()

    else:
        print('sys.argv', sys.argv, 'NO ACTION')
        pass
