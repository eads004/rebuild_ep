#!/usr/bin/env python

import glob, re

year_totals = {}
total_totals = 0

for p in glob.glob('/home/spenteco/0/rebuild_ep_data/ngrams/unigramsByYear/*.csv'):
    
    inData = open(p, 'r', encoding='utf-8').read().split('\n')

    for i in inData:
        
        if i.strip() > '':
        
            cols = i.strip().split('\t')
            
            if len(cols) == 8:
        
                year = cols[-3]
                n = int(cols[-2])
                
                if year not in year_totals:
                    year_totals[year] = 0
                    
                year_totals[year] += n
                total_totals += n
            
print()
for year in sorted(year_totals.keys()):
    print(year, year_totals[year])
print()

print('total_totals', total_totals)
