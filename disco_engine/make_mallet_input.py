#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, glob, json, re
from lxml import etree
from nltk.corpus import stopwords

def is_number_etc(w):

    result = False

    if '.' in w:
        result = True
    elif re.sub('[0-9]', '', w) != w:
        result = True 
    elif re.sub('[ivx]', '', w.lower()) == '':
        result = True 
    elif w == '{non-roman}':
        result = True 

    return result

#sw = set(stopwords.words('english') + ['thee', 'thy', 'thou', 'thine'] + ['a','ab','ac','ad','at','atque','aut','autem','cum','de','dum','e','erant','erat','est','et','etiam','ex','haec','hic','hoc','in','ita','me','nec','neque','non','per','qua','quae','quam','qui','quibus','quidem','quo','quod','re','rebus','rem','res','sed','si','sic','sunt','tamen','tandem','te','ut','vel'])

sw = set([])

bogus_letters = set([u'•', u'…', u'〈', u'〉'])

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = sys.argv[1]

MALLET_INPUT_FILE_NAME = sys.argv[2] + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'

output_file = open(MALLET_INPUT_FILE_NAME, 'w', encoding='utf-8')
for f in sorted(glob.glob(MORPHADORNER_FOLDER + '**/*.xml', recursive=True)):

    tokens = [f.split('/')[-1].split('.')[0], 'en']

    tree = etree.parse(f)

    for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        if w.get('lemma') != None and w.get('lemma') > '' and w.get('lemma') not in sw and '|' not in w.get('lemma') and is_number_etc(w.get('lemma')) == False:
            is_bogus = False
            for c in bogus_letters:
                if c in w.get('lemma'):
                    is_bogus = True
            if is_bogus == False:
                tokens.append(w.get('lemma').lower())

    if len(tokens) > 2:
        output_file.write(' '.join(tokens) + '\n')

output_file.close()
