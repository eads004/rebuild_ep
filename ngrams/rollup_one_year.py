#!/usr/bin/env python

import os, sys, re, csv, sqlite3
import time
#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

def processOneYear(metadataDatabase, year, inputFolder, outputFolder):

    startingTime = time.time()

    print('processOneYear', 'year', year, type(year), 'inputFolder', inputFolder, 'outputFolder', outputFolder)
        
    conn = sqlite3.connect(metadataDatabase)

    c = conn.cursor()
    
    c.execute('SELECT eebo_tcp_key FROM metadata WHERE year = ?', (year,))

    allGrams = {}
    
    fileNames = c.fetchall()

    #print('PROCESSING', 'year', year, 'fileNames', fileNames)
    
    for f in fileNames:
        
        fileName = f[0]
        
        try:
         
            inData = open(inputFolder + fileName + '.tsv', 'r', encoding='utf-8').read().split('\n')
            
            #print('loading file ' + fileName + ' for year ' + str(year) + ' ' + inputFolder)
            
            fileGrams = {}

            for i in inData:
                
                if i.strip() > '':
                
                    cols = i.strip().split('\t')
                    
                    if len(cols) > 2:
                    
                        try:
                        
                            gram = tuple(cols[:-2])
                            n = int(cols[-1])

                            try:
                                fileGrams[gram] += n
                            except KeyError:
                                fileGrams[gram] = n
                                
                        except:
                            print('BAD ROW (A)', i)
                    else:
                        print('BAD ROW (B)', i)
                        
            for k, v in fileGrams.items():
                try:
                    allGrams[k]['wf'] += v
                    allGrams[k]['df'] += 1
                except KeyError:
                    allGrams[k] = {'wf': 0, 'df': 0}
                    allGrams[k]['wf'] += v
                    allGrams[k]['df'] += 1
        
        except IOError:
            pass

    outF = open(outputFolder + str(year) + '.csv', 'w', encoding='utf-8')
    for k in allGrams.keys():
        outF.write('\t'.join(k) + '\t' + str(year) + '\t' + str(allGrams[k]['wf']) + '\t' + str(allGrams[k]['df']) + '\n')
    outF.close()

    endingTime = time.time()

    print('processed ' + str(year) + ' ' + inputFolder + ' in ' + str(endingTime - startingTime) + ' seconds. len(allGrams)', len(allGrams))

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

metadataDatabase = sys.argv[1]
year = int(sys.argv[2])
gramType = sys.argv[3]    

connA = sqlite3.connect(metadataDatabase)

cA = connA.cursor()

cA.execute('SELECT distinct year FROM metadata;')
    
years = cA.fetchall()

rootFolder = '/home/spenteco/0/rebuild_ep_data/ngrams/'

processOneYear(metadataDatabase, year, rootFolder + 'raw_ngram_data/' + gramType + '/', rootFolder + gramType + 'ByYear/')
    

