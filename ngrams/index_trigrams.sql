CREATE INDEX trigrams_1 ON trigrams (tok1, pos1, tok2, pos2, tok3, pos3);
CREATE INDEX trigrams_2 ON trigrams (spe1, pos1, spe2, pos2, spe3, pos3);
CREATE INDEX trigrams_3 ON trigrams (reg1, pos1, reg2, pos2, reg3, pos3);
CREATE INDEX trigrams_4 ON trigrams (lem1, pos1, lem2, pos2, lem3, pos3);
    
