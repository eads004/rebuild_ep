#!/usr/bin/env python
## coding: utf-8

import sys, glob, json, re

LIKENESS_DEPTH = 'text'

MALLET_INPUT_FILE_NAME = sys.argv[1] + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'

df = {}

with open(MALLET_INPUT_FILE_NAME, 'r', encoding='utf-8') as f:
    for line in f:

        tokens = line.strip().lower().split(' ')

        unique_tokens = list(set(tokens[2:]))
        for t in unique_tokens:
            try:
                df[t] += 1
            except KeyError:
                df[t] = 1

output_f = open(sys.argv[1] + 'mallet_df.json', 'w', encoding='utf-8')
output_f.write(json.dumps(df, indent=4))
output_f.close()
