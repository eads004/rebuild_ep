#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, random
from collections import defaultdict, Counter
from lxml import etree

def print_dictionary(d, dname):

    print
    print dname
    print
    for w in Counter(d).most_common():
        print '\t', w[0], w[1]

# ----------------------------------------------------------------------

#MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
MORPHADORNER_FOLDER = 'test_adorned/'

p1_counts = defaultdict(int)
p2_counts = defaultdict(int)
p3_counts = defaultdict(int)
in_out_counts = defaultdict(int)

for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

    # BAD XML FROM MORPHADORNER
    if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

        print 'processing', f.split('/')[-1].split('.')[0]

        tree = etree.parse(f)

        for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):

            #in_out_nodes = w.xpath('ancestor::tei:lg|ancestor::tei:p|ancestor::tei:head|ancestor::tei:epigraph', 
            #                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
            in_out_nodes = w.xpath('ancestor::tei:lg|ancestor::tei:p|ancestor::tei:epigraph', 
                                    namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})

            if len(in_out_nodes) == 0:
                in_out_counts['out'] += 1

                p1 = w.getparent()
                p2 = p1.getparent()
                p3 = p2.getparent()

                p1_tag = p1.tag.replace('{http://www.tei-c.org/ns/1.0}', '')
                p2_tag = p2.tag.replace('{http://www.tei-c.org/ns/1.0}', '')
                p3_tag = p3.tag.replace('{http://www.tei-c.org/ns/1.0}', '')

                #print p1_tag, p2_tag, p3_tag

                p1_counts[p1_tag] += 1
                p2_counts[p2_tag] += 1
                p3_counts[p3_tag] += 1

            else:
                in_out_counts['in'] += 1

print_dictionary(in_out_counts, 'in-out')
print_dictionary(p1_counts, 'p1')
print_dictionary(p2_counts, 'p2')
print_dictionary(p3_counts, 'p3')
