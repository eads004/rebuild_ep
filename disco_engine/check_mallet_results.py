#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

FOLDER = '/home/spenteco/0/rebuild_ep_data/disco_engine/'
N_TOPICS = 200

# ----------------------------------------------------------------------

import re

labels = []
dense_matrix = []

f = open(FOLDER + 'all_eebo.mallet.text.TRIMMED.INPUT.doc_topics.tsv')

n_read = 0

for line in f:
    if line.strip() > '':
        
        c = re.split('\s+', line.strip())
        
        if len(c) > N_TOPICS:
            labels.append(c[1])
            
            row = []
            for n in c[2:]:
                row.append(float(n))
                
            dense_matrix.append(row)
        
f.close()

print('len(labels)', len(labels))
print('len(dense_matrix)', len(dense_matrix))

# ----------------------------------------------------------------------

import json

f = open(FOLDER + 'all_eebo.mallet.text.LABELS.json', 'w', encoding='utf-8')
f.write(json.dumps(labels))
f.close()

f = open(FOLDER + 'all_eebo.mallet.text.DENSE_MATRIX.json', 'w', encoding='utf-8')
f.write(json.dumps(dense_matrix))
f.close()

# ----------------------------------------------------------------------

import gensim
from gensim import corpora
import numpy as np

lda_corpus = gensim.matutils.Dense2Corpus(np.array(dense_matrix).T)

corpora.MmCorpus.serialize(FOLDER + 'all_eebo.mallet.text.LDA_CORPUS', lda_corpus) 

# ----------------------------------------------------------------------

from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile

lda_corpus = corpora.MmCorpus(FOLDER + 'all_eebo.mallet.text.LDA_CORPUS')

index_tmpfile = get_tmpfile(FOLDER + 'all_eebo.mallet.text.TMP.index')

index = similarities.Similarity(index_tmpfile, lda_corpus, num_features=N_TOPICS)

index.save(FOLDER + 'all_eebo.mallet.text.index')

print('make_index', 'DONE')

# ----------------------------------------------------------------------

print(len(dense_matrix))
print(len(dense_matrix[0]))
print(len(lda_corpus))
print(len(lda_corpus[0]))
