#!/usr/bin/env python
## coding: utf-8

import re

def is_number_etc(w):

    result = False

    if '.' in w:
        result = True
    elif re.sub('[0-9]', '', w) != w:
        result = True 
    elif re.sub('[ivx]', '', w.lower()) == '':
        result = True 


    print w, result

    return result


# -----------------------------------------------

is_number_etc('bob')
is_number_etc('123')
is_number_etc('a.b.')
is_number_etc('II')
is_number_etc('VII')
is_number_etc('VII.')
