<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 
    version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:template match="tei:w|tei:pc">
        <xsl:copy-of select="." />
    </xsl:template>

</xsl:stylesheet>
