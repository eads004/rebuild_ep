#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, glob, re, json, re, time, sys
from gensim import corpora, models, similarities, matutils

FOLDER = '/home/spenteco/0/rebuild_ep_data/disco_engine/'
N_TOPICS = 200

METADATA_FILE_NAME = 'EEBO_metadata.tsv'
MALLET_LABELS_FILE_NAME = FOLDER + 'all_eebo.mallet.text.LABELS.json'
MALLET_CORPUS_FILE_NAME = FOLDER + 'all_eebo.mallet.text.LDA_CORPUS'
    
# ----------------------------------------------------------------------

def load_metadata():

    metadata = {}

    for line in open(METADATA_FILE_NAME, 'r', encoding='utf-8').read().split('\n'):
        if line.strip() > '' and len(line.strip().split('\t')) > 3:
            cols = line.strip().split('\t')
            
            metadata[cols[0]] = {'year': cols[1], 'author': cols[2], 'title': cols[3], 
                                 'subject': cols[4], 'phase': cols[6]}

    return metadata

def load_labels_json(file_name):

    file_name_labels = json.loads(open(file_name, 'r', encoding='utf-8').read())

    labels_hash = {}
    for n, f in enumerate(file_name_labels):
        labels_hash[f.split('.')[0]] = n

    return file_name_labels, labels_hash
    
# ----------------------------------------------------------------------

metadata = load_metadata()
    
# ----------------------------------------------------------------------

mallet_labels = load_labels_json(MALLET_LABELS_FILE_NAME)
mallet_corpus = corpora.MmCorpus(MALLET_CORPUS_FILE_NAME)

print('len(metadata)', len(metadata))
print()
print('len(mallet_labels)', len(mallet_labels))
print('len(mallet_corpus)', len(mallet_corpus))
print('len(mallet_corpus[0])', len(mallet_corpus[0]))
    
# ----------------------------------------------------------------------

f = open('MALLET_input_to_LargeViz.txt', 'w')

dictionary_size = len(mallet_corpus[0])

corpus = mallet_corpus

f.write(str(len(mallet_corpus)) + ' ' + str(dictionary_size) + '\n')

for cn, c in enumerate(mallet_corpus):
    
    if cn % 1000 == 0:
        print('processing', cn)

    matrix = matutils.corpus2dense([c], dictionary_size)
    matrix = matrix.T

    output_line = []
    for n in matrix[0]:
        output_line.append(str(n))
        
    f.write(' '.join(output_line) + '\n')
    
f.close()
    
# ----------------------------------------------------------------------

import subprocess, os

pwd = os.getcwd()

cmd = "cd ~/Downloads/LargeVis/Linux; ./LargeVis -outdim 2 -input " + pwd +  "/MALLET_input_to_LargeViz.txt -output " + pwd +  "/MALLET_output_from_LargeViz.txt"

print()
print(cmd)
print()
print(subprocess.getoutput(cmd))
    
# ----------------------------------------------------------------------

import json

f = open('big_lookup_hash.js', 'r', encoding='utf-8')
big_lookup_hash = json.loads(f.read())
f.close()

f = open('big_reverse_lookup_hash.js', 'r', encoding='utf-8')
reverse_lookup_hash = json.loads(f.read())
f.close()
    
# ----------------------------------------------------------------------

from collections import defaultdict, Counter

graph_data = []
subject_words = defaultdict(int)
years = defaultdict(int)

for n, l in enumerate(open('MALLET_output_from_LargeViz.txt', 'r').read().split('\n')[1:]):
    cols = l.split(' ')
    if len(cols) == 2:
        
        x = float(cols[0])
        y = float(cols[1])
        
        label = mallet_labels[0][n]
        
        m = {'phase': 'None', 
                'title': 'None', 
                 'author': 'None', 
                 'subject': 'None', 
                 'year': 'None'}
        try:
            m = metadata[label]
            
            for w in re.split('--|\|', m['subject']):
                if w.strip() > '':
                    fixed_w = w.strip()
                    if fixed_w.endswith('.'):
                        fixed_w = fixed_w[:-1]
                    subject_words[fixed_w] += 1
            
            years[m['year']] += 1
            
        except KeyError:
            print('KeyError', label)
            
        lookup_values = {}
        
        for k in big_lookup_hash.keys():
            lookup_values[k] = []
        
        for k, metadata_types in reverse_lookup_hash.items():
            if k in m['subject']:
                for metadata_type in metadata_types:
                    lookup_values[metadata_type].append(k)
                    
        for k in lookup_values.keys():
            lookup_values[k] = list(set(lookup_values[k]))
                        
        graph_data.append([label, x, y, m, lookup_values])
        
years_for_interface = []
for k, v in years.items():
    if k >= '1470' and k <= '1699':
        years_for_interface.append([k, v])
        
years_for_interface.sort()
        
subjects_for_interface = []
for k, v in subject_words.items():
    if v >= 50:
        subjects_for_interface.append([v, k])
        
subjects_for_interface.sort(reverse=True)
        
print()
print('len(graph_data)', len(graph_data))
print()
print(graph_data[0])
print()
print(graph_data[5][-1])
print()
print('len(years)', len(years))
print('len(subject_words)', len(subject_words))
print()
print('len(years_for_interface)', len(years_for_interface))
print('len(subjects_for_interface)', len(subjects_for_interface))
    
# ----------------------------------------------------------------------
        
import json

f = open('map_data.js', 'w', encoding='utf-8')
f.write(json.dumps([graph_data, years_for_interface, subjects_for_interface, big_lookup_hash]))
f.close()

print('Done!')
    
# ----------------------------------------------------------------------
        




