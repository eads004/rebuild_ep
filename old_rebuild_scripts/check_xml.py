#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re
from lxml import etree

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'

n_okay = 0
n_bad = 0

for path_to_file in glob.glob(MORPHADORNER_FOLDER + '*.xml'):
    try:
        tree = etree.parse(path_to_file)
        n_okay += 1
    except etree.XMLSyntaxError:
        print 'ERROR', path_to_file
        n_bad += 1

print
print 'n_okay', n_okay
print 'n_bad', n_bad
