
FIRST ROUND
===========

./tfidf_everything.py all

    Input is morphadorner tabular output.  It uses the regularized forms of words.  There's no filtering for punctuation; they get included.

./find_top_50.py

    Inputs are a) file labels; b) serialized gensim tf-idf corpus; c) serialized gensim index.  Note that the serialized gensim index depends on files which were written to /tmp, and which, because they are on /tmp, will go missing.

    Output is a csv which lists, for every file in the corpus, the 50 files which are most alike.

tail -n 3016350 OUT.find_top_50.tsv > CLEAN.OUT.find_top_50.tsv

    Removes nohup message and header from file.

sqlite3 tfidf_similarities.sqlite3

    Define table; import CLEAN.OUT.find_top_50.tsv:

        define_table.sql

    .separator "\t"

    .import CLEAN.OUT.find_top_50.tsv similarities

mv tfidf_similarities.sqlite3 tfidf_prototype

SECOND ROUND
============

    On ada

        /home/data/eebo_process/adorned

            60331 texts

    On my work station:

        /home/spenteco/0/eebo_012617

            60379 texts

./tfidf_everything.py all

    Input is morphadorner tabular output.  It uses the regularized forms of words.  There's no filtering for punctuation; they get included.

./find_top_50.py

    Inputs are a) file labels; b) serialized gensim tf-idf corpus; c) serialized gensim index.  Note that the serialized gensim index depends on files which were written to /tmp, and which, because they are on /tmp, will go missing.

    Output is a csv which lists, for every file in the corpus, the 50 files which are most alike.

TO DO: Replace find_top_50.py with something that loads the index and returns all things with a similarity score > some value.

TO DO: What values?

ON ADA:

    export PATH=/home/spenteco/anaconda2/bin:$PATH

    conda activate really_py2

    nohup ./tfidf_everything.py all > OUT_tfidf_everything.txt 2>&1 &

    conda install gensim

I stumbled across a file (A01463.xml) with a node like

    <w lem="matter" pos="n1" reg="matter" spe="matter" xml:id=*A01463-220310">matter</w>




