<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:ep="http://earlyprint.org/ns/1.0"
    version="2.0">
    
    <xsl:template match="tei:w">
        <xsl:copy>
            <xsl:if test="not(@reg)">
                <xsl:attribute name="reg" select="."/>
            </xsl:if>
            <xsl:if test="not(@orig)">
                <xsl:attribute name="orig" select="."/>
            </xsl:if>
            <xsl:if test="not(@lemma)">
                <xsl:choose>
                    <xsl:when test="not(@reg) or (@reg eq '')">
                        <xsl:attribute name="lemma"><xsl:value-of select="."/></xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="lemma"><xsl:value-of select="@reg"/></xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="element()">
        <xsl:copy>
            <xsl:apply-templates select="@*, node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="attribute() | text() | comment()">
        <xsl:copy/>
    </xsl:template>


</xsl:stylesheet>
