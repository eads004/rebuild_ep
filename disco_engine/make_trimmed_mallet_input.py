#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, glob, json, re
from lxml import etree

UPPER_LIMIT = 33000
#UPPER_LIMIT = 20000
LOWER_LIMIT = 10
#LOWER_LIMIT = 100

df = json.loads(open(sys.argv[1] + 'mallet_df.json', 'r', encoding='utf-8').read())

good_words = []

n_gt_upper_limit = 0
n_le_lower_limit = 0

for k, v in df.items():
    if v >= UPPER_LIMIT:
        n_gt_upper_limit += 1
    elif v <= LOWER_LIMIT:
        n_le_lower_limit += 1
    else:
        if re.search('[0-9]', k) == None:
            good_words.append(k)
            
print('n_gt_upper_limit', n_gt_upper_limit)
print('n_le_lower_limit', n_le_lower_limit)
print('len(good_words)', len(good_words))

good_words = set(good_words)

LIKENESS_DEPTH = 'text'

INPUT_FILE_NAME = sys.argv[1] + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'
OUTPUT_FILE_NAME = sys.argv[1] + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.TRIMMED.INPUT.txt'

input_file = open(INPUT_FILE_NAME, 'r', encoding='utf-8')

output_file = open(OUTPUT_FILE_NAME, 'w', encoding='utf-8')

for line in input_file:
    
    if line.strip() > '':
        
        cols = re.split('\s+', line.strip())

        tokens = []
        for c in cols[2:]:
            if c in good_words:
                tokens.append(c)

        output_file.write(cols[0] + ' ' + cols[1] + ' ' + ' '.join(tokens) + '\n')

input_file.close()
output_file.close()
