#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8
    
# ----------------------------------------------------------------------
    
import glob
from lxml import etree

big_lookup_hash = {}

for p in glob.glob('metadata_values/*.xml'):
    
    metadata_type = p.split('/')[-1].split('.')[0]
    
    big_lookup_hash[metadata_type] = []
    
    tree = etree.parse(p)
    
    for i in tree.xpath('//div[@class="item"]'):
        n = int(i.xpath('descendant::span[@class="count"]')[0].text)
        label = i.xpath('descendant::span[@class="label"]')[0].text
        
        if n >= 50:
        
            big_lookup_hash[metadata_type].append((n, label))
            
for k in big_lookup_hash.keys():
    big_lookup_hash[k] = sorted(list(set(big_lookup_hash[k])), reverse=True)
    
reverse_lookup_hash = {}
            
for k in big_lookup_hash.keys():
    for v in big_lookup_hash[k]:
        try:
            reverse_lookup_hash[v[1]].append(k)
        except KeyError:
            reverse_lookup_hash[v[1]] = [k]
    
# ----------------------------------------------------------------------
        
import json

f = open('big_lookup_hash.js', 'w', encoding='utf-8')
f.write(json.dumps(big_lookup_hash))
f.close()

f = open('big_reverse_lookup_hash.js', 'w', encoding='utf-8')
f.write(json.dumps(reverse_lookup_hash))
f.close()

print('Done!')
    
# ----------------------------------------------------------------------
