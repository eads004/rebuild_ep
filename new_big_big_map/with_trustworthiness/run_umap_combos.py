#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

from gensim import corpora, models, matutils
import umap
from sklearn.preprocessing import StandardScaler
import json

DATA_FOLDER = '/home/spenteco/0/rebuild_ep_data/lsa_umap/'

f = open(DATA_FOLDER + 'all_eebo.lsi_umap.labels.json', 'r', encoding='utf-8')
labels = json.load(f)
f.close()

for n_topics in [25, 50, 100, 200]:

    corpus_lsi = corpora.MmCorpus(DATA_FOLDER + 'all_eebo.lsi_umap.lsa.' + str(n_topics))

    matrix = matutils.corpus2dense(corpus_lsi, n_topics)
    matrix = matrix.T

    for n_neighbors in (25, 50, 100, 200):
        
        for min_dist in (0.0, 0.5, 0.99):
            
            for metric in ['euclidean',
                            'cosine']:

                result_file = 'UMAP.n_topics_' + str(n_topics) + \
                            '_n_neighbors_' + str(n_neighbors) + \
                            '_min_dist_' + str(min_dist) + \
                            '_metric_' + str(metric) + \
                            '.json'

                #try:
                if True:
                
                    reducer = umap.UMAP(n_neighbors=n_neighbors, min_dist=min_dist, metric=metric)

                    scaled_matrix = StandardScaler().fit_transform(matrix)

                    embedding = reducer.fit_transform(scaled_matrix)
                    
                    result = []
                    
                    for a in range(0, len(embedding)):

                        #print('a', a, 'labels[a]', labels[a], embedding[a])
                    
                        row = {}
                        row['tcp_id'] = labels[a][0].split('.')[0]
                        row['x'] = float(embedding[a][0])
                        row['y'] = float(embedding[a][1])
                    
                        result.append(row)
        
                    f = open(DATA_FOLDER + result_file, 'w', encoding='utf-8')
                    f.write(json.dumps(result, indent=4))
                    f.close()
                    
                    print('OK', result_file)
                    
                #except:
                    #print('ERROR', result_file)
                    
                    
                    
