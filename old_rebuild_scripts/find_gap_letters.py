#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, string
from lxml import etree
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile
from nltk.corpus import stopwords
from collections import defaultdict, Counter

bogus_letters = set([u'•', u'…', u'〈', u'〉'])

DICTIONARY_FILE_NAME = '/home/data/all_3_prototype/all_eebo.tfidf.text.dict'

my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

letter_counts = defaultdict(int)

for k, v in my_dictionary.iteritems():
    #for b in bogus_letters:
    #    if b in v:
    #        print v
    #        break
    for c in v:
        letter_counts[c] += 1

print
for w in Counter(letter_counts).most_common():
    if w[0] in string.letters:
        pass
    else:
        print w[0], w[1]
