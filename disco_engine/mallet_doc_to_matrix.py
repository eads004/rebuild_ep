#!/home/spenteco/anaconda2/envs/py3/bin/python
## coding: utf-8

import sys, glob, json, re, csv
from lxml import etree
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile

# ----------------------------------------------------------------------

INPUT_FILE_NAME = sys.argv[1]
OUTPUT_FOLDER = sys.argv[2]
N_TOPICS = 200

# ----------------------------------------------------------------------

LIKENESS_DEPTH = 'text'

LABELS_FILE_NAME = OUTPUT_FOLDER + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.labels.json'

DICTIONARY_FILE_NAME = OUTPUT_FOLDER + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.dict'
MM_CORPUS_FILE_NAME = OUTPUT_FOLDER + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.mm'
INDEX_FILE_NAME = OUTPUT_FOLDER + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.index'
TMP_FILE_NAME = OUTPUT_FOLDER + 'all_eebo.mallet.' + LIKENESS_DEPTH + '.TMP.index'

# ----------------------------------------------------------------------

def make_index(mm_corpus):

    index = similarities.Similarity(TMP_FILE_NAME, 
                                    mm_corpus, 
                                    num_features=N_TOPICS)

    index.save(INDEX_FILE_NAME)
    
# ----------------------------------------------------------------------

def load_mallet_docs_tsv():
    
    labels = []
    mm_corpus = []
    
    f = open(INPUT_FILE_NAME, 'r', encoding='utf-8')
    
    read_tsv = csv.reader(f, delimiter='\t')

    for row in read_tsv:
      
        labels.append([row[1], row[1]])
        
        mm_row = []
        for a in range(2, len(row)):
            mm_row.append((a - 2, row[a]))
            
        mm_corpus.append(mm_row)

    f.close()
    
    return labels, mm_corpus
    
# ----------------------------------------------------------------------

if __name__ == "__main__":
    
    labels, mm_corpus = load_mallet_docs_tsv()

    f = open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(labels))
    f.close()

    f = open(MM_CORPUS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(mm_corpus))
    f.close()
    
    make_index(mm_corpus)
    
    print('DONE')
