#!/home/spenteco/anaconda2/envs/py3/bin/python
## -*- coding: utf-8 -*-

import sys, glob, re

#   --------------------------------------------------------------------
#
#   -------------------------------------------------------------------- 

if __name__ == "__main__":

    INPUT_FOLDER = sys.argv[1]
    
    all_token_counts = 0

    for n, path_to_file in enumerate(glob.glob(INPUT_FOLDER + '/*.txt')):

        tcp_id = path_to_file.split('/')[-1].split('.')[0]
        
        original_tokens = []
        scrubbed_tokens = []
        
        for line in open(path_to_file, 'r', encoding='utf-8').read().split('\n'):
            if line.strip() > '':
                original_tokens.append(line)
                
                temp = [p for p in re.split('\s+', line.strip()) if p > '']
                
                if len(temp) > 2:
                    if temp[0] == temp[2]:
                        #print('DROPPED', line)
                        pass
                    else:
                        scrubbed_tokens.append(line)
                    
        all_token_counts += len(scrubbed_tokens)
                    
        print(tcp_id, len(scrubbed_tokens))
                
    print()
    print('all_token_counts', all_token_counts)
