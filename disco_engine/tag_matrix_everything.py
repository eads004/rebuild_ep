#!/usr/bin/env python
## coding: utf-8

import sys, glob, json, re
from lxml import etree
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = sys.argv[1]

LABELS_FILE_NAME = sys.argv[2] + 'all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.labels.json'
DICTIONARY_FILE_NAME = sys.argv[2] + 'all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.dict'
MM_CORPUS_FILE_NAME = sys.argv[2] + 'all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.mm'
TAG_MATRIX_CORPUS_FILE_NAME = sys.argv[2] + 'all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.tag_matrix'
INDEX_FILE_NAME = sys.argv[2] + 'all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.index'
TMP_FILE_NAME = sys.argv[2] + '/all_eebo.tag_matrix.' + LIKENESS_DEPTH + '.TMP.index'

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            tokens = []

            tree = etree.parse(file_name[1])

            for w in tree.xpath('//*', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                tag_value = w.tag.replace('{http://www.tei-c.org/ns/1.0}', '')
                if tag_value not in ['w', 'c', 'pc', 'gap', 'TEI', 'text']:
                    tokens.append(tag_value)

            yield tokens
        
# ----------------------------------------------------------------------

def get_labels():

    file_name_labels = []
    
    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '**/*.xml', recursive=True)):
        file_name_labels.append([f.split('/')[-1], f])

    f = open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(file_name_labels))
    f.close()

    print('get_labels', 'len(file_name_labels)', len(file_name_labels))

def load_labels_json():

    file_name_labels = json.loads(open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    print('load_labels_json', 'len(file_name_labels)', len(file_name_labels))

    return file_name_labels

def make_dictionary():
    
    print('make_dictionary', 'STARTED')

    file_name_labels = load_labels_json()

    texts = Morphadorned_Corpus(file_name_labels)
    
    my_dictionary = corpora.Dictionary(texts)

    my_dictionary.save(DICTIONARY_FILE_NAME)
    
    print('make_dictionary', 'DONE')

def make_tag_matrix():
    
    print('make_tag_matrix', 'STARTED')

    file_name_labels = load_labels_json()

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    texts = Morphadorned_Corpus(file_name_labels)

    mm_corpus = [my_dictionary.doc2bow(text) for text in texts]

    corpora.MmCorpus.serialize(MM_CORPUS_FILE_NAME, mm_corpus)

    model = models.normmodel.NormModel(mm_corpus, norm='l1')

    corpus_tag_matrix = []
    for m in mm_corpus:
        corpus_tag_matrix.append(model[m])

    corpora.MmCorpus.serialize(TAG_MATRIX_CORPUS_FILE_NAME, corpus_tag_matrix)
    
    print('make_tag_matrix', 'DONE')

def make_index():
    
    print('make_index', 'STARTED')

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    corpus_tag_matrix = corpora.MmCorpus(TAG_MATRIX_CORPUS_FILE_NAME)

    index_tmpfile = get_tmpfile(TMP_FILE_NAME)

    index = similarities.Similarity(index_tmpfile, corpus_tag_matrix, num_features=len(my_dictionary))

    index.save(INDEX_FILE_NAME)
    
    print('make_index', 'DONE')

if __name__ == "__main__":
    
    if sys.argv[3] == 'all':

        get_labels()
        make_dictionary()
        make_tag_matrix()
        make_index()

    elif sys.argv[3] == 'labels':
        get_labels()

    elif sys.argv[3] == 'dictionary':
        make_dictionary()

    elif sys.argv[3] == 'tag_matrix':
        make_tag_matrix()

    elif sys.argv[3] == 'index':
        make_index()

    else:
        print('sys.argv', sys.argv, 'NO ACTION')
        pass
