#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re
from lxml import etree
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile

LIKENESS_DEPTH = 'text'

#MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
MORPHADORNER_FOLDER = 'test_adorned/'

#LABELS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.labels.json'
#DICTIONARY_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.dict'
#MM_CORPUS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.mm'
#LLR_CORPUS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.llr'
#INDEX_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.index'
#TMP_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.TMP.index'

LABELS_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.labels.json'
DICTIONARY_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.dict'
MM_CORPUS_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.mm'
LLR_CORPUS_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.llr'
INDEX_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.index'
TMP_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.TMP.index'

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            tokens = []

            tree = etree.parse(MORPHADORNER_FOLDER + file_name)

            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if w.get('reg') != None and w.get('reg') > '':
                    tokens.append(w.get('reg').lower())

            yield tokens
        
# ----------------------------------------------------------------------

def get_labels():

    file_name_labels = []

    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

        # BAD XML FROM MORPHADORNER
        if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                    'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                    'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                    'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                    'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                    'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

            file_name_labels.append(f.split('/')[-1])

    f = codecs.open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(file_name_labels))
    f.close()

    print 'get_labels', 'len(file_name_labels)', len(file_name_labels)

def load_labels_json():

    file_name_labels = json.loads(codecs.open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    print 'load_labels_json', 'len(file_name_labels)', len(file_name_labels)

    return file_name_labels

def make_dictionary():
    
    print 'make_dictionary', 'STARTED'

    file_name_labels = load_labels_json()

    texts = Morphadorned_Corpus(file_name_labels)
    
    my_dictionary = corpora.Dictionary(texts)

    my_dictionary.save(DICTIONARY_FILE_NAME)
    
    print 'make_dictionary', 'DONE'

def make_llr():
    
    print 'make_llr', 'STARTED'

    file_name_labels = load_labels_json()

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    texts = Morphadorned_Corpus(file_name_labels)

    mm_corpus = [my_dictionary.doc2bow(text) for text in texts]

    corpora.MmCorpus.serialize(MM_CORPUS_FILE_NAME, mm_corpus)

    texts_total_words = []
    type_counts = {}

    for mm in mm_corpus:
        total_words = 0
        for w in mm:
            if w[0] not in type_counts:
                type_counts[w[0]] = 0
            type_counts[w[0]] += w[1]
            total_words += w[1]
        texts_total_words.append(total_words)

    total_word_count_in_corpus = 0
    for t in texts_total_words:
        total_word_count_in_corpus += t

    print
    print type_counts
    print
    print texts_total_words
    print
    print 'total_word_count_in_corpus', total_word_count_in_corpus

    #llr = models.TfidfModel(mm_corpus)

    #corpus_llr = llr[mm_corpus]

    #corpora.MmCorpus.serialize(LLR_CORPUS_FILE_NAME, corpus_llr)
    
    print 'make_llr', 'DONE'

def make_index():
    
    print 'make_index', 'STARTED'

    my_dictionary = corpora.Dictionary.load(DICTIONARY_FILE_NAME)

    corpus_llr = corpora.MmCorpus(LLR_CORPUS_FILE_NAME)

    index_tmpfile = get_tmpfile(TMP_FILE_NAME)

    index = similarities.Similarity(index_tmpfile, corpus_llr, num_features=len(my_dictionary))

    index.save(INDEX_FILE_NAME)
    
    print 'make_index', 'DONE'

if __name__ == "__main__":
    
    if sys.argv[1] == 'all':

        get_labels()
        make_dictionary()
        make_llr()
        #make_index()

    elif sys.argv[1] == 'labels':
        get_labels()

    elif sys.argv[1] == 'dictionary':
        make_dictionary()

    elif sys.argv[1] == 'llr':
        make_llr()

    elif sys.argv[1] == 'index':
        make_index()

    else:
        print 'sys.argv', sys.argv, 'NO ACTION'
        pass
