#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, sys, json
from lxml import etree

INPUT_FOLDER = sys.argv[1]
OUTPUT_FILE = sys.argv[2]

authors = []
titles = []

for a, p in enumerate(glob.glob(INPUT_FOLDER + '*.xml')):
    
    if a % 100 == 0:
        print('processing', a)
        
    tei = etree.parse(p)
    
    for a in tei.xpath('//tei:titleStmt/tei:author', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        authors.append(a.text)
        break
    
    for t in tei.xpath('//tei:search_title', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        titles.append(t.text)
        break
        
authors = sorted(list(set(authors)))
titles = sorted(list(set(titles)))

print(len(authors))
print(len(titles))

f = open(OUTPUT_FILE, 'w', encoding='utf-8')
f.write(json.dumps({'authors': authors, 'titles': titles}))
f.close()

print('Done!')
