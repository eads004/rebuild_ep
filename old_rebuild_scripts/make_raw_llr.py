#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, math
from lxml import etree
from collections import Counter
from nltk.corpus import stopwords

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
#MORPHADORNER_FOLDER = 'test_adorned/'

LABELS_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.labels.json'
LLR_FILE_NAME = '/home/data/all_3_prototype/all_eebo.llr.' + LIKENESS_DEPTH + '.raw_llr'

#LABELS_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.labels.json'
#LLR_FILE_NAME = 'test_outputs/all_eebo.llr.' + LIKENESS_DEPTH + '.raw_llr'

sw = set(stopwords.words('english') + ['thy', 'thou', 'thine'])

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            print 'loading', file_name

            tokens = []

            tree = etree.parse(MORPHADORNER_FOLDER + file_name)

            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if w.get('lem') != None and w.get('lem') > '':

                    l = w.get('lem').lower()
                    if l == re.sub('[^a-z]', '', l) and l not in sw:
                        tokens.append(w.get('lem').lower())

            yield tokens
        
# ----------------------------------------------------------------------

def get_labels():

    file_name_labels = []

    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

        # BAD XML FROM MORPHADORNER
        if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                    'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                    'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                    'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                    'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                    'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

            file_name_labels.append(f.split('/')[-1])

    f = codecs.open(LABELS_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(file_name_labels))
    f.close()

    print 'get_labels', 'len(file_name_labels)', len(file_name_labels)

def load_labels_json():

    file_name_labels = json.loads(codecs.open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    print 'load_labels_json', 'len(file_name_labels)', len(file_name_labels)

    return file_name_labels

def make_llr():
    
    print 'make_llr', 'STARTED'

    file_name_labels = load_labels_json()

    texts = Morphadorned_Corpus(file_name_labels)

    # http://wordhoard.northwestern.edu/userman/analysis-comparewords.html

    A_text_word_counts = []   
    B_corpus_word_counts = {}
    C_text_word_count_totals = [] 
    D_corpus_total_words = 0    

    for text in texts:

        A_text_word_counts.append(Counter(text))
        
        total_n = 0        
        for w, n in A_text_word_counts[-1].iteritems():

            total_n += n

            if w not in B_corpus_word_counts:
                B_corpus_word_counts[w] = 0
            B_corpus_word_counts[w] += n

        C_text_word_count_totals.append(total_n)

    for n in C_text_word_count_totals:
        D_corpus_total_words += n
            
    text_word_llr_scores = []
    for ai, a in enumerate(A_text_word_counts):
        
        word_llr_scores = {}
        for wa, na in a.iteritems():
    
            E1 = C_text_word_count_totals[ai] * float(na + B_corpus_word_counts[wa]) / \
                    float(C_text_word_count_totals[ai] + D_corpus_total_words)
            E2 = D_corpus_total_words * float(na + B_corpus_word_counts[wa]) / \
                    float(C_text_word_count_totals[ai] + D_corpus_total_words)
            G2 = 2 * ((na * math.log(na / E1)) + \
                    (B_corpus_word_counts[wa] * math.log(B_corpus_word_counts[wa] / E2)))

            word_llr_scores[wa] = G2

        text_word_llr_scores.append(word_llr_scores)

    #for i, word_llr_scores in enumerate(text_word_llr_scores[:1]):
        
    #    print
    #    print file_name_labels[i]
    #    print
    #    for w in Counter(word_llr_scores).most_common(  ):
    #        print '\t', w[0], w[1]

    f = codecs.open(LLR_FILE_NAME, 'w', encoding='utf-8')
    f.write(json.dumps(text_word_llr_scores))
    f.close()

    print 'make_llr', 'len(text_word_llr_scores)', len(text_word_llr_scores)

    #print
    #print A_text_word_counts[0]
    #print
    #print B_corpus_word_counts
    #print
    #print C_text_word_count_totals
    #print
    #print D_corpus_total_words
    #print

if __name__ == "__main__":

    get_labels()
    make_llr()
