CREATE INDEX bigrams_1 ON bigrams (tok1, pos1, tok2, pos2);
CREATE INDEX bigrams_2 ON bigrams (spe1, pos1, spe2, pos2);
CREATE INDEX bigrams_3 ON bigrams (reg1, pos1, reg2, pos2);
CREATE INDEX bigrams_4 ON bigrams (lem1, pos1, lem2, pos2);
    
