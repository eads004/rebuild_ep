#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, math
from lxml import etree
from collections import Counter

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
#MORPHADORNER_FOLDER = 'test_adorned/'

WORD2VEC_INPUT_FILE_NAME = '/home/data/all_3_prototype/all_eebo.WORD2VEC_INPUT.' + LIKENESS_DEPTH
#WORD2VEC_INPUT_FILE_NAME = 'test_outputs/all_eebo.WORD2VEC_INPUT.' + LIKENESS_DEPTH

def get_labels():

    file_name_labels = []

    for f in sorted(glob.glob(MORPHADORNER_FOLDER + '/*.xml')):

        # BAD XML FROM MORPHADORNER
        if f.split('/')[-1] not in ['A01463.xml', 'A03448.xml', 'A06411.xml', 
                                    'A06415.xml', 'A06433.xml', 'A08659.xml', 
                                    'A10077.xml', 'A11267.xml', 'A20579.xml', 
                                    'A21312.xml', 'A22608.xml', 'A33551.xml', 
                                    'A53720.xml', 'A55752.xml', 'A57689.xml', 
                                    'A69234.xml', 'A84893.xml', 'A90811.xml', ]:

            file_name_labels.append(f.split('/')[-1])

    return file_name_labels

# ----------------------------------------------------------------------

class Morphadorned_Corpus(object):

    def __init__(self, file_name_labels):

        self.file_name_labels = file_name_labels

    def __iter__(self):

        for file_name in self.file_name_labels:

            print 'loading', file_name

            tokens = []

            tree = etree.parse(MORPHADORNER_FOLDER + file_name)

            for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                if w.get('lem') != None and w.get('lem') > '':

                    l = w.get('lem').lower()
                    if l == re.sub('[^a-z]', '', l):
                        tokens.append(w.get('lem').lower())

            yield tokens
        
# ----------------------------------------------------------------------

def make_WORD2VEC_INPUT():
    
    print 'make_WORD2VEC_INPUT', 'STARTED'

    f = codecs.open(WORD2VEC_INPUT_FILE_NAME, 'w', encoding='utf-8')

    texts = Morphadorned_Corpus(get_labels())

    A_text_word_counts = []   

    for text in texts:
        f.write(' '.join(text) + '\n')

    f.close()

    print 'make_WORD2VEC_INPUT', 'len(A_text_word_counts)', len(A_text_word_counts)

if __name__ == "__main__":

    make_WORD2VEC_INPUT()
