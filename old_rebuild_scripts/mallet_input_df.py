#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, random
from lxml import etree

LIKENESS_DEPTH = 'text'

MALLET_INPUT_FILE_NAME = '/home/data/all_3_prototype_rebuild/all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'

df = {}

with codecs.open(MALLET_INPUT_FILE_NAME, 'r', encoding='utf-8') as f:
    for line in f:

        tokens = line.strip().lower().split(' ')

        print tokens[0]

        unique_tokens = list(set(tokens[2:]))
        for t in unique_tokens:
            try:
                df[t] += 1
            except KeyError:
                df[t] = 1

output_f = codecs.open('/home/data/all_3_prototype_rebuild/mallet_df.json', 'w', encoding='utf-8')
output_f.write(json.dumps(df, indent=4))
output_f.close()
