#!/home/spenteco/anaconda2/envs/py3/bin/python 

import sys, os, re, csv, sqlite3, time, glob
from lxml import etree

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

inputFolder = sys.argv[1]
metadataDatabase = sys.argv[2]
unigramFolder = sys.argv[3]
bigramFolder = sys.argv[4]
trigramFolder = sys.argv[5]
        
conn = sqlite3.connect(metadataDatabase)
c = conn.cursor()

for inputFile in sorted(glob.glob(inputFolder + '**/*.xml', recursive=True)):

    #if 'A00018' not in inputFile:
    #    continue
    
    startTime = time.time()
    
    tcp_id = inputFile.split('/')[-1].split('.')[0]
    
    print('processing', inputFile)
    
    c.execute('SELECT year FROM metadata WHERE eebo_tcp_key = ?', (tcp_id,))
    rows = c.fetchall()
    
    year = -1

    if rows == None or len(rows) == 0:
        #continue
        pass
    else:
        year = rows[0][0]
    
    tree = etree.parse(inputFile)
    
    inLines = []
    
    for w in tree.xpath('//tei:w', namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
        
        tok = ''
        if w.text != None:
            tok = re.sub('\s+', ' ', w.text.strip())
            
        spe = ''
        if w.text != None:
            spe = re.sub('\s+', ' ', w.text.strip())
            
        pos = ''
        if w.get('pos') != None:
            pos = w.get('pos')
            
        reg = ''
        if w.get('reg') != None:
            reg = w.get('reg')
        else:
            reg = tok
            
        lem = ''
        if w.get('lemma') != None:
            lem = w.get('lemma')
            
        eos = ''
        
        inLines.append([tok, spe, pos, reg, lem, eos])
    
    unigrams = {}

    for i in range(0, len(inLines)):
        
        cols1 = inLines[i]
        
        if len(cols1) == 6:
            
            tok1 = cols1[0].lower()
            spe1 = cols1[1].lower()
            pos1 = cols1[2].lower()
            reg1 = cols1[3].lower()
            lem1 = cols1[4].lower()
            eos1 = cols1[5].lower()
            
            try:
                unigrams[(tok1, spe1, pos1, reg1, lem1)] += 1
            except KeyError:
                unigrams[(tok1, spe1, pos1, reg1, lem1)] = 1
                
    outF = open(unigramFolder + tcp_id + '.tsv', 'w', encoding='utf-8')
    for k in sorted(unigrams.keys()):
        outF.write('\t'.join(k) + '\t' + str(year) + '\t' + str(unigrams[k]) + '\n')
    outF.close()
    
    outF.close()
    
    bigrams = {}

    for i in range(0, len(inLines) - 1):
        
        cols1 = inLines[i]
        cols2 = inLines[i + 1]
        
        if len(cols1) == 6 and len(cols2) == 6:
            
            tok1 = cols1[0].lower()
            spe1 = cols1[1].lower()
            pos1 = cols1[2].lower()
            reg1 = cols1[3].lower()
            lem1 = cols1[4].lower()
            eos1 = cols1[5].lower()
            
            tok2 = cols2[0].lower()
            spe2 = cols2[1].lower()
            pos2 = cols2[2].lower()
            reg2 = cols2[3].lower()
            lem2 = cols2[4].lower()
            eos2 = cols2[5].lower()
            
            try:
                bigrams[(tok1, spe1, pos1, reg1, lem1, tok2, spe2, pos2, reg2, lem2)] += 1
            except KeyError:
                bigrams[(tok1, spe1, pos1, reg1, lem1, tok2, spe2, pos2, reg2, lem2)] = 1
                
    outF = open(bigramFolder + tcp_id + '.tsv', 'w', encoding='utf-8')
    for k in sorted(bigrams.keys()):
        outF.write('\t'.join(k) + '\t' + str(year) + '\t' + str(bigrams[k]) + '\n')
    outF.close()
    
    trigrams = {}

    for i in range(0, len(inLines) - 2):
        
        cols1 = inLines[i]
        cols2 = inLines[i + 1]
        cols3 = inLines[i + 2]
        
        if len(cols1) == 6 and len(cols2) == 6 and len(cols3) == 6:
            
            tok1 = cols1[0].lower()
            spe1 = cols1[1].lower()
            pos1 = cols1[2].lower()
            reg1 = cols1[3].lower()
            lem1 = cols1[4].lower()
            eos1 = cols1[5].lower()
            
            tok2 = cols2[0].lower()
            spe2 = cols2[1].lower()
            pos2 = cols2[2].lower()
            reg2 = cols2[3].lower()
            lem2 = cols2[4].lower()
            eos2 = cols2[5].lower()
            
            tok3 = cols3[0].lower()
            spe3 = cols3[1].lower()
            pos3 = cols3[2].lower()
            reg3 = cols3[3].lower()
            lem3 = cols3[4].lower()
            eos3 = cols3[5].lower()
            
            try:
                trigrams[(tok1, spe1, pos1, reg1, lem1, tok2, spe2, pos2, reg2, lem2, tok3, spe3, pos3, reg3, lem3)] += 1
            except KeyError:
                trigrams[(tok1, spe1, pos1, reg1, lem1, tok2, spe2, pos2, reg2, lem2, tok3, spe3, pos3, reg3, lem3)] = 1
                
    outF = open(trigramFolder + tcp_id + '.tsv', 'w', encoding='utf-8')
    for k in sorted(trigrams.keys()):
        outF.write('\t'.join(k) + '\t' + str(year) + '\t' + str(trigrams[k]) + '\n')
    outF.close()
    
    stopTime = time.time()
    
    print('DONE', inputFile, (stopTime - startTime))

    
