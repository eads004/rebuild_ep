
Prereqs
=======

1.  Quite a bit of disk space.  500G?

2.  A fair amount of time.  Three or four days?  It's mostly watching things run in the background.

3.  Python 3 and LXML.

4.  A copy of the blacklab-1.7.3.jar.  There's a hard-coded path to this jar in blacklab/prep/index_PROD.sh.

5.  A copy of saxon9he.jar.  There's a hard-coded path to this jar in blacklab/prep/index_PROD.sh.


Inputs
======

Requires local copies of [the eebotcp/earlyprint corpus](https://bitbucket.org/eplib/eebotcp/src/master/) and [the earlyprint metadata](https://github.com/earlyprint/epmetadata)

**title_xref.csv**, which cross-references original title spellings and regularized title spellings, and which is in this repo.


Outputs
=======

The processes write their outputs to a set of folders like (and which need to be created before the processes run):

    rebuild_ep_data/

        rebuild_ep_data/blacklab
            rebuild_ep_data/blacklab/temp
            rebuild_ep_data/blacklab/temp2

        rebuild_ep_data/disco_engine

        rebuild_ep_data/ngrams
            rebuild_ep_data/ngrams/shortcuts
            rebuild_ep_data/ngrams/raw_ngram_data
            rebuild_ep_data/ngrams/raw_ngram_data/unigrams
            rebuild_ep_data/ngrams/raw_ngram_data/bigrams
            rebuild_ep_data/ngrams/raw_ngram_data/trigrams
            rebuild_ep_data/ngrams/unigramsByYear
            rebuild_ep_data/ngrams/bigramsByYear
            rebuild_ep_data/ngrams/trigramsByYear

The results will be scp'd to the various servers (disco and ngrams to talus, blacklab to the eplab AWS instance.


Ngrams
======

### Proving the numbers

    pull_test_set.ipynb

    Then, **make metadata sql database*.

    **Generate ngrams** using /home/spenteco/0/eebotcp_test/texts/ as input

    **Handwork for databases**, like below


### make metadata sql database ###

    sqlite3 /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_metadata.sqlite3
            
        create table metadata (eebo_tcp_key text, year integer, author text, title text);

        create table savedQueries (queryLabel text, savedQuery text);
        
        insert into savedQueries values ('love,loue', 'http://earlyprint.wustl.edu/tooleebospellingbrowserv2.html?requestFromClient={"1":{"spe":"love,loue","reg":"","lem":"","pos":"","originalPos":""},"2":{"spe":"","reg":"","lem":"","pos":"","originalPos":""},"3":{"spe":"","reg":"","lem":"","pos":"","originalPos":""},"databaseType":"unigrams","smoothing":"False","rollingAverage":"none"}');

    ./generateMetadata.py /home/spenteco/0/epmetadata/header/ > EEBO_epmetadata.tsv

    ./loadMetadata.py /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_metadata.sqlite3 EEBO_epmetadata.tsv

### Generate ngrams ###

    nohup python -u eeboToCsv.py /home/spenteco/0/eebotcp/texts/  /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_metadata.sqlite3 /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/unigrams/ /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/bigrams/ /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/trigrams/  > OUT_eeboToCsv.txt 2>&1 &

## Rollup the year-by-year numbers ##

Folders need to be created:

    bigramsByYear
    trigramsByYear
    unigramsByYear

The run the script:
    
    nohup ./rollup_script.sh > OUT_rollup_script.sh 2>&1 &
    
## Lots of hand-work to create and load the three databases ##

    cd /home/spenteco/0/rebuild_ep_data/ngrams

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_unigrams.sqlite3
    
    sqlite3 eebo_tcp_unigrams.sqlite3
       
    create table unigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, year integer, wf integer, df integer);

    .exit

    cd /home/spenteco/3/rebuild_ep/ngrams

    export PYTHONIOENCODING=utf-8

    nohup python -u loadGrams.py /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_unigrams.sqlite3 unigrams /home/spenteco/0/rebuild_ep_data/ngrams/unigramsByYear/ > OUT_loadGrams_1.txt 2>&1 &

    ----------------------------------------------------------------

    cd /home/spenteco/0/rebuild_ep_data/ngrams

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_bigrams.sqlite3
    
    sqlite3 eebo_tcp_bigrams.sqlite3
        
    create table bigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text,year integer,  wf integer, df integer);

    .exit

    cd /home/spenteco/3/rebuild_ep/ngrams

    export PYTHONIOENCODING=utf-8

    nohup python -u loadGrams.py /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_bigrams.sqlite3 bigrams /home/spenteco/0/rebuild_ep_data/ngrams/bigramsByYear/ > OUT_loadGrams_2.txt 2>&1 &

    ----------------------------------------------------------------

    cd /home/spenteco/0/rebuild_ep_data/ngrams

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_trigrams.sqlite3
    
    sqlite3 eebo_tcp_trigrams.sqlite3
        
    create table trigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text, tok3 text, spe3 text, pos3 text, reg3 text, lem3 text, year integer,  wf integer, df integer);

.exit

    cd /home/spenteco/3/rebuild_ep/ngrams

    export PYTHONIOENCODING=utf-8

    nohup python -u loadGrams.py /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_trigrams.sqlite3 trigrams /home/spenteco/0/rebuild_ep_data/ngrams/trigramsByYear/ > OUT_loadGrams_3.txt 2>&1 &

    ----------------------------------------------------------------

    cd /home/spenteco/0/rebuild_ep_data/ngrams
    
    sqlite3 eebo_tcp_unigrams.sqlite3
        
    CREATE INDEX unigrams_1 ON unigrams (tok1, pos1);
    CREATE INDEX unigrams_2 ON unigrams (spe1, pos1);
    CREATE INDEX unigrams_3 ON unigrams (reg1, pos1);
    CREATE INDEX unigrams_4 ON unigrams (lem1, pos1);

.exit

    sqlite3 eebo_tcp_bigrams.sqlite3
        
    CREATE INDEX bigrams_1 ON bigrams (tok1, pos1, tok2, pos2);
    CREATE INDEX bigrams_2 ON bigrams (spe1, pos1, spe2, pos2);
    CREATE INDEX bigrams_3 ON bigrams (reg1, pos1, reg2, pos2);
    CREATE INDEX bigrams_4 ON bigrams (lem1, pos1, lem2, pos2);

.exit

    sqlite3 eebo_tcp_trigrams.sqlite3
        
    CREATE INDEX trigrams_1 ON trigrams (tok1, pos1, tok2, pos2, tok3, pos3);
    CREATE INDEX trigrams_2 ON trigrams (spe1, pos1, spe2, pos2, spe3, pos3);
    CREATE INDEX trigrams_3 ON trigrams (reg1, pos1, reg2, pos2, reg3, pos3);
    CREATE INDEX trigrams_4 ON trigrams (lem1, pos1, lem2, pos2, lem3, pos3);

.exit
    
    sqlite3 eebo_tcp_unigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM unigrams GROUP BY 1;

.exit
    
    sqlite3 eebo_tcp_bigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM bigrams GROUP BY 1;

.exit
    
    sqlite3 eebo_tcp_trigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM trigrams GROUP BY 1;

.exit
    
## Shortcuts for regex searches ##
 
    ./regexShortcut.py /home/spenteco/0/rebuild_ep_data/ngrams/eebo_tcp_unigrams.sqlite3 /home/spenteco/0/rebuild_ep_data/ngrams/shortcuts/
 

Disco
=====

    nohup ./tfidf_everything.py /home/spenteco/0/eebotcp/ /home/spenteco/0/rebuild_ep_data/disco_engine/ all > OUT.tfidf_everything.txt 2>&1 &

    nohup ./tag_matrix_everything.py /home/spenteco/0/eebotcp/ /home/spenteco/0/rebuild_ep_data/disco_engine/ all > OUT.tag_matrix_everything.txt 2>&1 &

    nohup ./do_mallet.sh > OUT.do_mallet.txt 2>&1 &

    ./extract_metadata_categories.py

    ./check_mallet_results.py

    ./matrix_to_LargeVis.py
    
        ** TODO: NEEDS GRAPH CODE MOVED HERE (OR SOMEWHERE) FROM old_rebuild_scripts NOTEBOOK **




/home/spenteco/0/rebuild_ep_data/disco_engine

Blacklab
========

    rebuild_ep_data/

        rebuild_ep_data/blacklab
            rebuild_ep_data/blacklab/temp
            rebuild_ep_data/blacklab/temp2

        rebuild_ep_data/disco_engine

        rebuild_ep_data/ngrams
            rebuild_ep_data/ngrams/shortcuts
            
    nohup python add_metadata.py /home/spenteco/0/eebotcp/ /home/spenteco/0/rebuild_ep_data/blacklab/temp/ /home/spenteco/0/epmetadata/header/ > OUT.add_metadata.txt 2>&1 &

    nohup prep/index_PROD.sh > OUT.index_PROD.txt 2>&1 &

    nohup prep/index_MAC.sh > OUT.index_PROD.txt 2>&1 &




