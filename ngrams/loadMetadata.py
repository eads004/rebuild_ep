#!/home/spenteco/anaconda2/envs/py3/bin/python

import sys, os, re, csv, sqlite3

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

conn = sqlite3.connect(sys.argv[1])

c = conn.cursor()

inLines = open(sys.argv[2], 'r', encoding='utf-8').read().split('\n')

for line in inLines:
    
    cols = line.split('\t')
    
    if len(cols) > 3:
        
        eebo_tcp_key = cols[0]
        year = ''
        try:
            year = int(cols[1])
        except:
            pass
        author = cols[2]
        title = cols[3]

        c.execute('INSERT INTO metadata VALUES (?, ?, ?, ?)', (eebo_tcp_key, year, author, title))
                
conn.commit()
