
************************************************************************
SQLITE3 REBUILD PROCESSES
************************************************************************

    60327 XML files
    60284 morphadorned files

            LOST 43.  WHICH ONES?  WHY?
    
    60327 UNIQUE metadata keys (61785 TOTAL)

    1,621,023,698 tokens

     
    **** unigrams in final set of data
    **** bigrams 
    **** trigrams

    **** unigrams in sqlite
    **** bigrams 
    **** trigrams

1.  Create a metadata database

    cd /home/spenteco/0/eebo_012617

    mkdir eeboNgramSqliteDbsV3    
    cd eeboNgramSqliteDbsV3

    sqlite3 eebo_tcp_metadata.sqlite3
        
    create table metadata (eebo_tcp_key text, year integer, author text, title text, was_committed_to_database integer);

    create table savedQueries (queryLabel text, savedQuery text);
    
    insert into savedQueries values ('love,loue', 'http://earlyprint.wustl.edu/tooleebospellingbrowserv2.html?requestFromClient={"1":{"spe":"love,loue","reg":"","lem":"","pos":"","originalPos":""},"2":{"spe":"","reg":"","lem":"","pos":"","originalPos":""},"3":{"spe":"","reg":"","lem":"","pos":"","originalPos":""},"databaseType":"unigrams","smoothing":"False","rollingAverage":"none"}');

    cd /home/spenteco/1/earlyprint.wustl.edu/rebuild_METADATA

    ./loadMetadata.py /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_metadata.sqlite3 EEBO_metadata.tsv

2.  Generate ngrams

    cd /home/data2/eebo_tcp_MARCH_2015/rebuild_ngram_db

    nohup python -u eeboToCsv.py /home/spenteco/0/eebotcp/ /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_metadata.sqlite3 /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/unigrams/ /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/bigrams/ /home/spenteco/0/rebuild_ep_data/ngrams/raw_ngram_data/trigrams/  > OUT_eeboToCsv.txt 2>&1 &

        unigrams    6.5G
        bigrams     40G
        trigrams    91G
    
3.  Roll up by year
    
    nohup ./rollup_script.sh > OUT_rollup_script.sh 2>&1 &


3.  Import into sql

    #   ----------------------------------------------------------------

    cd /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_unigrams.sqlite3
    
    sqlite3 eebo_tcp_unigrams.sqlite3
       
    create table unigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, year integer, wf integer, df integer);

    cd /home/spenteco/1/earlyprint.wustl.edu/rebuild_NGRAM

    export PYTHONIOENCODING=utf-8

    nohup python -u loadGrams.py /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_unigrams.sqlite3 unigrams /home/spenteco/0/eebo_012617/raw_ngram_data/unigramsByYear/ > OUT_loadGrams_1.txt 2>&1 &

    #   ----------------------------------------------------------------

    cd /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_bigrams.sqlite3
    
    sqlite3 eebo_tcp_bigrams.sqlite3
        
    create table bigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text,year integer,  wf integer, df integer);

    cd /home/spenteco/1/earlyprint.wustl.edu/rebuild_NGRAM

    export PYTHONIOENCODING=utf-8
    
    nohup python -u loadGrams.py /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_bigrams.sqlite3 bigrams /home/spenteco/0/eebo_012617/raw_ngram_data/bigramsByYear/ > OUT_loadGrams_2.txt 2>&1 &

    #   ----------------------------------------------------------------

    cd /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3

    cp eebo_tcp_metadata.sqlite3 eebo_tcp_trigrams.sqlite3
    
    sqlite3 eebo_tcp_trigrams.sqlite3
        
    create table trigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text, tok3 text, spe3 text, pos3 text, reg3 text, lem3 text, year integer,  wf integer, df integer);

    cd /home/spenteco/1/earlyprint.wustl.edu/rebuild_NGRAM

    export PYTHONIOENCODING=utf-8
    
    nohup python -u loadGrams.py /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_trigrams.sqlite3 trigrams /home/spenteco/0/eebo_012617/raw_ngram_data/trigramsByYear/ > OUT_loadGrams_3.txt 2>&1 &

4.  Create indexes
    
    sqlite3 eebo_tcp_unigrams.sqlite3
        
    CREATE INDEX unigrams_1 ON unigrams (tok1, pos1);
    CREATE INDEX unigrams_2 ON unigrams (spe1, pos1);
    CREATE INDEX unigrams_3 ON unigrams (reg1, pos1);
    CREATE INDEX unigrams_4 ON unigrams (lem1, pos1);

    sqlite3 eebo_tcp_bigrams.sqlite3
        
    CREATE INDEX bigrams_1 ON bigrams (tok1, pos1, tok2, pos2);
    CREATE INDEX bigrams_2 ON bigrams (spe1, pos1, spe2, pos2);
    CREATE INDEX bigrams_3 ON bigrams (reg1, pos1, reg2, pos2);
    CREATE INDEX bigrams_4 ON bigrams (lem1, pos1, lem2, pos2);

    sqlite3 eebo_tcp_trigrams.sqlite3
        
    CREATE INDEX trigrams_1 ON trigrams (tok1, pos1, tok2, pos2, tok3, pos3);
    CREATE INDEX trigrams_2 ON trigrams (spe1, pos1, spe2, pos2, spe3, pos3);
    CREATE INDEX trigrams_3 ON trigrams (reg1, pos1, reg2, pos2, reg3, pos3);
    CREATE INDEX trigrams_4 ON trigrams (lem1, pos1, lem2, pos2, lem3, pos3);
        
5.  Rollup by year
    
    sqlite3 eebo_tcp_unigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM unigrams GROUP BY 1;
    
    sqlite3 eebo_tcp_bigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM bigrams GROUP BY 1;
    
    sqlite3 eebo_tcp_trigrams.sqlite3
    
    create table totals_by_year (year integer, wf integer);
    
    INSERT INTO totals_by_year SELECT year, SUM(wf) FROM trigrams GROUP BY 1;
 
6.  regex shortcut

    ./regexShortcut.py /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/eebo_tcp_unigrams.sqlite3 /home/spenteco/0/eebo_012617/eeboNgramSqliteDbsV3/shortcuts/
 
7.  Transfer data from ADA to TALUS; wire up, etc

************************************************************************

    OLD?

8.  New morphdorner output database.  
************************************************************************

    cd /home/data/eeboNgramSqliteDbsV2

    cp eebo_tcp_metadata.sqlite3 morphadorner_data.sqlite3
    
    sqlite3 morphadorner_data.sqlite3
       
    create table morphadorner_data (eebo_id text, tok text, spe text, pos text, reg text, lem text, sentence_end integer);

        See https://www.sqlite.org/autoinc.html re id numbers in sqlite3.

    cd /home/data/eebo_tcp_MARCH_2015/rebuild_ngram_db

    nohup python -u loadMorphadorner.py /home/data/eeboNgramSqliteDbsV2/morphadorner_data.sqlite3 ../morphadorned/ > OUT_loadMorphadorner.txt 2>&1 &
        
    cd /home/data/eeboNgramSqliteDbsV2/

    sqlite3 morphadorner_data.sqlite3

    drop table savedQueries;

CREATE INDEX morphadorner_data_1 ON morphadorner_data (eebo_id);
CREATE INDEX morphadorner_data_2 ON morphadorner_data (tok);
CREATE INDEX morphadorner_data_3 ON morphadorner_data (spe);
CREATE INDEX morphadorner_data_4 ON morphadorner_data (pos);
CREATE INDEX morphadorner_data_5 ON morphadorner_data (reg);
CREATE INDEX morphadorner_data_6 ON morphadorner_data (lem);


9.  Modify interface to work with the slightly different data model;

    a.  Apache directive

        <Directory /home/spenteco/1/eeboNgrams/eeboSqllite/eeboSpellingBrowserV2>
        Require all granted
        </Directory>

        WSGIScriptAlias /eebospellingbrowserv2 /home/spenteco/1/eeboNgrams/eeboSqllite/eeboSpellingBrowserV2/EeboSpellingBrowser.py

    b.  Regex daemon

        i.  which ports are open?

            sudo nmap -sT -O localhost

        ii.  eeboRegexDaemon.sh start|stop|restart|status

                runs eeboRegexDamon.py

        iii. Move eeboRegexDaemon.sh to /etc/init.d

        iv.  Add eeboRegexDaemon.py to monit
        
12. Copy eeboSpellingBrowserV2 to eeboSpellingBrowserV3 in svn.
    Change all eeboSpellingBrowserV2 references to eeboSpellingBrowserV3.

13. Install eeboSpellingBrowserV3 on talus

    Configure apache

    Copy configs (2)

    EeboSpellingBrowser.py, eeboRegexDaemon.sh, eeboRegexDaemon.sh

    Copy eeboRegexDaemon.sh to /etc/init.d

    cp -r eeboSpellingBrowserStaticV3/ /home/data/htdocs


    http://earlyprint.wustl.edu/eebospellingbrowserv3

14. Test interface on talus.  Is it fast enough?  Do the results match earlyprint?

15. Line integration w/web grok.

    http://bl.ocks.org/mbostock/8033015




15. Move earlyprint (and ngram creation scripts and notes) to talus.

17. earlyprint.wustl.edu to svn

16. Remove old sqlite from ada (separate to-do).



************************************************************************
STEPS TO INSTALL AND CONFIG
************************************************************************

    Path in EeboSpellingBrowser
    config.py
    eeboSpellingBrowser.conf
    
    js/
    
    index.html?  cp index.html.8xxx index.html
    
    EeboSpellingBrowser changes to run local vs mod_wsgi



************************************************************************
LOCAL VS TALUS
************************************************************************

    eeboRegexDaemon.sh
    eeboRegexDaemon.py
    
    cp eeboSpellingBrowser.conf.WS eeboSpellingBrowser.conf

    cp config.py.WS config.py

************************************************************************
MOVE TO POSTGRES.
************************************************************************

    1.  Depends on "SQLITE3 REBUILD PROCESSES" below.

    2.  Make sure postgres is running.

        Change authentication from peer to md5    

        /etc/init.d/postgresql status

        sudo service postgresql stop

        sudo service postgresql start

        sudo -u postgres psql postgres

        sudo -u postgres createuser -s -P eebo_user

        sudo -u postgres createdb eebo_ngrams -O eebo_user

    3.  Define tables

        psql eebo_ngrams -U eebo_user -W

            g0cr
        
        create table metadata (eebo_tcp_key text, year integer, author text, title text, was_committed_to_database integer);
           
        create table unigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, year integer, wf integer, df integer);
            
        create table bigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text,year integer,  wf integer, df integer);
            
        create table trigrams (tok1 text, spe1 text, pos1 text, reg1 text, lem1 text, tok2 text, spe2 text, pos2 text, reg2 text, lem2 text, tok3 text, spe3 text, pos3 text, reg3 text, lem3 text, year integer,  wf integer, df integer);


    4.  Loaded ngrams on ada.

        ./load_unigrams_postgres.py   
        ./load_bigrams_postgres.py 
        ./load_trigrams_postgres.py   

    5.  Create indexes.
    
        psql eebo_ngrams -U eebo_user

        CREATE INDEX unigrams_2 ON unigrams (spe1);
        CREATE INDEX unigrams_1 ON unigrams (tok1);
        CREATE INDEX unigrams_3 ON unigrams (reg1);
        CREATE INDEX unigrams_4 ON unigrams (lem1);

        CREATE INDEX bigrams_1 ON bigrams (tok1, tok2);
        CREATE INDEX bigrams_2 ON bigrams (spe1, spe2);
        CREATE INDEX bigrams_3 ON bigrams (reg1, reg2);
        CREATE INDEX bigrams_4 ON bigrams (lem1, lem2);

        CREATE INDEX trigrams_1 ON trigrams (tok1, tok2, tok3);
        CREATE INDEX trigrams_2 ON trigrams (spe1, spe2, spe3);
        CREATE INDEX trigrams_3 ON trigrams (reg1, reg2, reg3);
        CREATE INDEX trigrams_4 ON trigrams (lem1, lem2, lem3);


        sudo apt-get install postgresql-contrib

        CREATE EXTENSION pg_trgm;
        CREATE INDEX unigrams_spe1 ON unigrams USING gist(spe1 gist_trgm_ops);

http://www.postgresql.org/docs/9.1/static/pgtrgm.html
http://www.postgresql.org/docs/9.3/static/pgtrgm.html
        
        CREATE INDEX unigrams_tok1 ON unigrams USING gist(tok1 gist_trgm_ops);
        CREATE INDEX unigrams_reg1 ON unigrams USING gist(reg1 gist_trgm_ops);
        CREATE INDEX unigrams_lem1 ON unigrams USING gist(lem1 gist_trgm_ops);
        CREATE INDEX unigrams_spe1 ON unigrams USING gist(spe1 gist_trgm_ops);

        Results in 293G in /var/lib/postgresql/9.1/main

    
    6.  Port application

        /etc/apache2/sites-available/000-default.conf

        http://localhost/eeboSpellingBrowserStaticV3/index.html

        sudo vi /etc/postgresql/9.1/main/postgresql.conf
        listen_addresses = '*'

        sudo chmod -R 700 /var/lib/postgresql/9.1/main

        sudo service postgresql restart

        sudo chmod -R 755 /var/lib/postgresql/9.1/main

        sudo vi /etc/postgresql/9.1/main/pg_hba.conf

    7.  UPGRADE postgres

        pg_dump -U eebo_user eebo_ngrams -f eebo_ngrams.sql

        sudo apt-get purge postgresql*
        sudo apt-get autoremove

        https://wiki.postgresql.org/wiki/Apt

        sudo apt-get install wget ca-certificates
        wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
        sudo apt-get update
        sudo apt-get upgrade

        sudo apt-get install postgresql-9.4 pgadmin3 postgresql-contrib-9.4

        sudo service postgresql start

        sudo -u postgres createuser -s -P eebo_user

        sudo -u postgres createdb eebo_ngrams -O eebo_user

        sudo vi /etc/postgresql/9.4/main/pg_hba.conf

        sudo vi /etc/postgresql/9.4/main/postgresql.conf

        vi /var/lib/pgsql/data/postgresql.conf

        sudo service postgresql restart

        psql -U eebo_user -d eebo_ngrams -f eebo_ngrams.sql

        psql eebo_ngrams -U eebo_user

    SECURITY IS F'D UP

        sudo iptables -L

        sudo iptables -A INPUT -p tcp -s 0/0 --sport 1024:65535 -d 128.252.89.228 --dport 5432 -m state --state NEW,ESTABLISHED -j ACCEPT
        sudo iptables -A OUTPUT -p tcp -s 128.252.89.228 --sport 5432 -d 0/0 --dport 1024:65535 -m state --state ESTABLISHED -j ACCEPT


        \d
        \d table_name

************************************************************************
BEFORE
************************************************************************

EXPLAIN SELECT spe1, year, wf FROM unigrams WHERE spe1 ~* '^lo[uv]e$';

BEFORE

                               QUERY PLAN                            
    -----------------------------------------------------------------
     Seq Scan on unigrams  (cost=0.00..718181.15 rows=3156 width=16)
       Filter: (spe1 ~* '^lo[uv]e$'::text)
    (2 rows)

AFTER

                                       QUERY PLAN                                    
    ---------------------------------------------------------------------------------
     Bitmap Heap Scan on unigrams  (cost=132.88..11802.55 rows=3156 width=16)
       Recheck Cond: (spe1 ~* '^lo[uv]e$'::text)
       ->  Bitmap Index Scan on unigrams_spe1  (cost=0.00..132.09 rows=3156 width=0)
             Index Cond: (spe1 ~* '^lo[uv]e$'::text)


EXPLAIN SELECT spe1, year, wf FROM unigrams WHERE spe1 in ('love', 'loue');

BEFORE

                                     QUERY PLAN                                 
        ----------------------------------------------------------------------------
         Bitmap Heap Scan on unigrams  (cost=14.72..645.99 rows=160 width=16)
           Recheck Cond: (spe1 = ANY ('{love,loue}'::text[]))
           ->  Bitmap Index Scan on unigrams_2  (cost=0.00..14.68 rows=160 width=0)
                 Index Cond: (spe1 = ANY ('{love,loue}'::text[]))
        (4 rows)

AFTER

                                         QUERY PLAN                                 
        ----------------------------------------------------------------------------
         Bitmap Heap Scan on unigrams  (cost=10.44..676.90 rows=169 width=16)
           Recheck Cond: (spe1 = ANY ('{love,loue}'::text[]))
           ->  Bitmap Index Scan on unigrams_2  (cost=0.00..10.40 rows=169 width=0)
                 Index Cond: (spe1 = ANY ('{love,loue}'::text[]))


number_of_requests 382
number_of_regexes 35

    20 Testing
    9 Error
    3 Part of speech
    3 Legitimate



************************************************************************
AFTER
************************************************************************

EXPLAIN SELECT spe1, year, wf FROM unigrams WHERE spe1 ~* '^lo[uv]e$';

                               QUERY PLAN                            
    -----------------------------------------------------------------
     Seq Scan on unigrams  (cost=0.00..718181.15 rows=3156 width=16)
       Filter: (spe1 ~* '^lo[uv]e$'::text)
    (2 rows)


EXPLAIN SELECT spe1, year, wf FROM unigrams WHERE spe1 in ('love', 'loue');

                                     QUERY PLAN                                 
    ----------------------------------------------------------------------------
     Bitmap Heap Scan on unigrams  (cost=13.96..645.22 rows=160 width=16)
       Recheck Cond: (spe1 = ANY ('{love,loue}'::text[]))
       ->  Bitmap Index Scan on unigrams_2  (cost=0.00..13.92 rows=160 width=0)
             Index Cond: (spe1 = ANY ('{love,loue}'::text[]))
    (4 rows)



************************************************************************
RARITY PROJECT
************************************************************************


            "The sequential scan scans the whole table sequentially . . . "
                PostgreSQL Developer's Guide, 2015
                http://catalog.wustl.edu:80/record=b5751853~S2


                Troubleshooting PostgreSQL, 2015
                http://catalog.wustl.edu:80/record=b5760490~S2

************************************************************************
RARITY PROJECT
************************************************************************

    1.  Special df tables for the rarity project

        nohup ./rollup_script_df.sh > OUT_rollup_script_df.txt 2>&1 &

        Creates a new folder of wf, df per year.

    2.  Create database

        cd /home/data/eebo_tcp_MARCH_2015

        cp eebo_tcp_metadata.sqlite3 unigram_df_tok1.sqlite3

        sqlite3 unigram_df_tok1.sqlite3
           
        create table tok1 (tok1 text, year integer, wf integer, df integer);

        cp eebo_tcp_metadata.sqlite3 unigram_df_reg1.sqlite3

        sqlite3 unigram_df_reg1.sqlite3
           
        create table reg1 (reg1 text, year integer, wf integer, df integer);

        cd /home/data/eebo_tcp_MARCH_2015

        nohup ./load_df.py unigram_df_tok1.sqlite3 tok1 raw_ngram_data/unigrams_tok1/ > OUT_load_df_tok1.txt 2>&1 &

        nohup ./load_df.py unigram_df_reg1.sqlite3 reg1 raw_ngram_data/unigrams_reg1/ > OUT_load_df_reg1.txt 2>&1 &

        sqlite3 unigram_df_tok1.sqlite3

        CREATE INDEX tok1_1 ON tok1 (tok1, year);

        sqlite3 unigram_df_reg1.sqlite3

        CREATE INDEX reg1_1 ON reg1 (reg1, year);


/^lo[uv]e.*$/,/^abo[uv]e$/
aboue,above,loue,loue·,loue*,louē,loue1,loue12,loue2,loue3
