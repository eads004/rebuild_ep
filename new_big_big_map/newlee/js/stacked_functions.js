
function calculate_offsets() {
    
    MY_N_CELLS = $('#n_cells').val();
    MY_OFFSETS = [];
    
    var lower_limit = Math.trunc(MY_N_CELLS / 2 * -1);
    var upper_limit = Math.trunc(MY_N_CELLS / 2);
    
    for (var a = lower_limit; a < upper_limit + 1; a ++) {
        MY_OFFSETS.push(a);
    }
}

function get_polygon_color(k, v) {
    
    var color = 0xffffff;
    
    /*
    var n = [0, 0];
    
    for (var a = 0; a < MY_SEARCH_WORDS.length; a++) {
        
        var words = MY_SEARCH_WORDS[a].toLowerCase().split('|');
        
        for (var b = 0; b < words.length; b++) {
            if (words[b].trim() > '') {
                n[a] += MY_LUNR[k].search(words[b].trim()).length;
            }
        }
    }
    
    if (n[0] > 0.0 && n[1] == 0.0) {
        color = 0xe57a82;
    }
    else {
        if (n[0] == 0.0 && n[1] > 0.0) {
            color = 0x857ae5;
        }
        else {
            if (n[0] > 0.0 && n[1] > 0.0) {
                color = 0xbb7ae5;
            }
        }
    }
    
    */
    
    //console.log('get_polygon_color', 'k', k, 'n', n, 'color', color);
    
    return color;
}

function load_slider() {
    
    $("#year_range").slider({
            range: true,
            min: 1470,
            max: 1700,
            values: [ 1470, 1700 ],
            slide: function( event, ui ) {
                
                MY_FROM_YEAR = parseInt(ui.values[0]);
                MY_TO_YEAR = parseInt(ui.values[1]);
                
                $("#year_label").html(
                        $("#year_range" ).slider("values", 0) +
                        " - " + 
                        $("#year_range" ).slider("values", 1));
    
                //$('#visualization').html('');
                
                //load_data();
            }
        }
    );
}
  
function calculate_breaks(from_value, to_value, n_bins, break_key) {
    
    var range = to_value - from_value;
    var span = (range / n_bins);
    
    MY_BREAKS[break_key] = [];
    
    for (var a = 0; a < n_bins; a++) {
        MY_BREAKS[break_key].push(from_value + (span * a));
    }
    
    MY_BREAKS[break_key].push(9999.99);
}
