#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, sys
from lxml import etree

INPUT_TEI = sys.argv[1]
OUTPUT_FOLDER = sys.argv[2]
INPUT_METADATA = sys.argv[3]

# ----------------------------------------------------------------------

import csv

title_xref = {}

f = open('title_xref.csv', 'r', encoding='utf-8')

r = csv.reader(f)

for row in r:
    title_xref[row[0].strip().replace(' ', '')] = row[1].strip()

f.close()

# ----------------------------------------------------------------------

processed_ids = []
for p in glob.glob(OUTPUT_FOLDER + '/*.xml'):
    processed_ids.append(p.split('/')[-1].split('.')[0])
    
processed_ids = set(processed_ids)

print('len(processed_ids)', len(processed_ids))

for a, p in enumerate(glob.glob(INPUT_TEI + '**/*.xml', recursive=True)):
    
    if a % 1000 == 0:
        print('processing', a)
        
    try:
    
        tcp_id = p.split('/')[-1].split('.')[0]
        
        if tcp_id in processed_ids:
            continue

        tei = etree.parse(p)
            
        original_date = ''
        search_year = ''
        display_year = ''
        
        if '_' in tcp_id:
            
            if len(tei.xpath('//ep:publicationYear', 
                            namespaces={'ep': 'http://earlyprint.org/ns/1.0'})) > 0:
                original_date = tei.xpath('//ep:publicationYear', 
                            namespaces={'ep': 'http://earlyprint.org/ns/1.0'})[0].text
            elif len(tei.xpath('//ep:creationYear', 
                            namespaces={'ep': 'http://earlyprint.org/ns/1.0'})) > 0:
                original_date = tei.xpath('//ep:creationYear', 
                            namespaces={'ep': 'http://earlyprint.org/ns/1.0'})[0].text
                            
            search_year = original_date
            display_year = original_date

        else:
            
            metadata = etree.parse(INPUT_METADATA + \
                                    tcp_id + '_sourcemeta.xml')

            root = tei.getroot()
            
            metadata_content = metadata.xpath('//tei:sourceDesc', 
                            namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[0]
                            
            dates = metadata.xpath('//tei:sourceDesc/tei:biblFull/tei:publicationStmt/tei:date', 
                            namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})
            
            when = dates[0].get('when')
            cert = dates[0].get('cert')
            notBefore = dates[0].get('notBefore')
            notAfter = dates[0].get('notAfter')
            
            original_date = dates[0].text
            if original_date == None:
                original_date = ''
            search_year = ''
            display_year = ''
            
            if notBefore != None and notBefore != '' and notAfter != None and notAfter != '':
                
                date_span = int(notAfter) - int(notBefore)
                if date_span > 0 and date_span <= 11:
                    search_year = str(int(notBefore) + int(date_span / 2))
                display_year = notBefore + '-' + notAfter
                
            elif when != None and when != '':
                search_year = when
                display_year = when
                if cert != None and cert != '':
                    display_year = display_year + '?'
            
        metadata_container = tei.xpath('//tei:teiHeader', 
                            namespaces={'tei': 'http://www.tei-c.org/ns/1.0'})[0]
        
        metadata_container.append(etree.fromstring('<original_date>' + \
                                                    original_date.replace('&', '&amp;') + \
                                                    '</original_date>'))
        
        metadata_container.append(etree.fromstring('<search_year>' + \
                                                    search_year.replace('&', '&amp;') + \
                                                    '</search_year>'))
        
        metadata_container.append(etree.fromstring('<display_year>' + \
                                                    display_year.replace('&', '&amp;') + \
                                                    '</display_year>'))
            
        # ----------------------------------------------------------

        for t in metadata_container.xpath('descendant::tei:title', 
                        namespaces={'tei': 'http://www.tei-c.org/ns/1.0'}):
                                
            new_title = ''
            
            try:
                            
                old_title = t.text.strip().replace(' ', '')
                new_title = title_xref[old_title].replace('&', '&amp;')
                
            except KeyError:
                print()
                print('\t', tcp_id, 'title keyError')
                print('\t', sys.exc_info()[0])
                print('\t', sys.exc_info()[1])
                print('\t', old_title)
            
            metadata_container.append(etree.fromstring('<search_title>' + \
                                                t.text.strip().replace('&', '&amp;') + \
                                                '|' + \
                                                new_title + \
                                                '</search_title>'))
            
            break

        # ----------------------------------------------------------

        tei.write(OUTPUT_FOLDER + '/' + tcp_id + '.xml')
        
    except:
        print()
        print('\t', tcp_id, 'problem?')
        print('\t', sys.exc_info()[0])
        print('\t', sys.exc_info()[1])
