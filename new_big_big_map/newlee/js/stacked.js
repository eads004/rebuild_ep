
    
var MY_N_CELLS = $('#n_cells').val();
var MY_OFFSETS = [];

var MY_ORIGINAL_DATA = [];
var MY_BOXES = [];

var MY_BREAKS = {'x': [], 'y': [], 'year': []};
var MY_BINNED_DATA = {};

var MAX_LENGTH = -1;
var MY_FROM_YEAR = 1470;
var MY_TO_YEAR = 1700;
var MY_SEARCH_WORDS = [];
var MY_LUNR = '';
var MY_N_TEXTS_SHOWN = 0;

var mouse = {x: 0, y: 0};
var MY_INTERSECTED_BOXES = [{x: null, y: null, z: null}]

var raycaster = new THREE.Raycaster();

document.addEventListener('mousemove', handle_mousemove, false );
document.addEventListener( 'mousedown', handle_mousedown, false );

$(function() {
    load_data();
    load_slider();
});
  
// FUNCTIONS, ETC

function handle_mousedown(event) {
    console.log('click', 'mouse', mouse, MY_INTERSECTED_BOXES[MY_INTERSECTED_BOXES.length - 1]);
}

function handle_mousemove(event)  {
	// event.preventDefault();
	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

function controls_changed() {
    
    calculate_offsets();
    
    MY_SEARCH_WORDS = [];
    
    if ($('#search_value_1').val().trim() > '') {
        MY_SEARCH_WORDS.push($('#search_value_1').val().toLowerCase());
    }
    
    if ($('#search_value_2').val().trim() > '') {
        MY_SEARCH_WORDS.push($('#search_value_2').val().toLowerCase());
    }
    
    $('#visualization').html('');
    
    load_data();
}

function find_bin(value, break_key) {
    
    //console.log('BEFORE', breaks);
 
    result = -1;
    for (var a = 0; a < MY_BREAKS[break_key].length - 1; a++) {
        
        if (value >= MY_BREAKS[break_key][a] && 
            value < MY_BREAKS[break_key][a + 1]) {
                
            result = a;
            break;
        }
    }
    
    //if (result == -1) {
    //    console.log('\t', value, break_key, MY_BREAKS[break_key], result);
    //}
    
    return result;
}
        
function load_data()  {
    
    MY_INTERSECTED_BOXES = [{x: null, y: null, z: null}]
    
    calculate_offsets();
    
    MY_N_TEXTS_SHOWN = 0;
    
    console.log("$('#input_csv').val()", $('#input_csv').val());
    
    $.getJSON('data/' + $('#input_csv').val(), 
        function( data ) {
            
            MY_ORIGINAL_DATA = data['MY_ORIGINAL_DATA'];
            MY_BOXES = data['BOXES'];
            
            MY_BREAKS = MY_BOXES[MY_N_CELLS]['MY_BREAKS'];
            MY_BINNED_DATA = MY_BOXES[MY_N_CELLS]['MY_BINNED_DATA'];

            MAX_LENGTH = -1;

            $.each(MY_BINNED_DATA, 
                function(key, value) {
                    if (value.n > MAX_LENGTH) {
                        MAX_LENGTH = value.n;
                    }
                }
            );

            var n_boxes = 0;
            var n_texts = 0;
            
            $.each(MY_BINNED_DATA,
                function(k, v) {
                    n_boxes += 1;
                    n_texts += v.n;
                }
            );
            
            console.log('n_boxes', n_boxes);
            console.log('n_texts', n_texts);
            
            console.log('done munging');
            
            crank_up_web_gl();
            animate();
        }
    );
}

function crank_up_web_gl() {
    
	// SCENE
	scene = new THREE.Scene();
    scene.background = new THREE.Color(0x504d47);
    
	// CAMERA
    
	var SCREEN_WIDTH = window.innerWidth;
    var SCREEN_HEIGHT = window.innerHeight;
    
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 9999;
    
	camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
	
    scene.add(camera)
	camera.position.set(100, (MY_N_CELLS * 40), -185);
	camera.lookAt(scene.position);
	
    // RENDERER
	if ( Detector.webgl )
		renderer = new THREE.WebGLRenderer({powerPreference: "low-power"} );
	else
		renderer = new THREE.CanvasRenderer(); 
        
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    
	container = document.getElementById('visualization' );
	container.appendChild(renderer.domElement);
	
    // EVENTS
	THREEx.WindowResize(renderer, camera);
	THREEx.FullScreen.bindKey({charCode : 'm'.charCodeAt(0)});
	
    // CONTROLS
	controls = new THREE.OrbitControls(camera, renderer.domElement);
	
    // STATS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.bottom = '0px';
	stats.domElement.style.zIndex = 100;
	container.appendChild( stats.domElement );
	
    // LIGHT
	var light = new THREE.PointLight(0xffffff);
	light.position.set(100, 1000, 0);
	scene.add(light);
    
	////////////
	// CUSTOM //
	////////////
    
    var polygon_geometry = new THREE.BoxGeometry(12, 12, 12);
    
    var n_boxes_with_data = 0;
    
    for (var xi = 0; xi < MY_OFFSETS.length; xi++) {
        
        var x = MY_OFFSETS[xi];
        
        for (var yi = 0; yi < MY_OFFSETS.length; yi++) {
            
            var y = MY_OFFSETS[yi];
            
            for (var zi = 0; zi < MY_OFFSETS.length; zi++) {
                
                var z = MY_OFFSETS[zi];
                
                var k = [xi, yi, zi];
                var v = MY_BINNED_DATA[k];
                
                if (v === undefined || v.length == 0) {
                }
                else {
                    
                    n_boxes_with_data += 1;
                    
                    var polygon_c = get_polygon_color(k, v);
                    
                    var polygon_opacity = 0.025 + (v.n / MAX_LENGTH);
                    
                    var polygon_material = new THREE.MeshBasicMaterial({color: polygon_c,
                                                                    wireframe: false,
                                                                    transparent: true, 
                                                                    opacity: polygon_opacity });
                    
                    var polygon = new THREE.Mesh(polygon_geometry.clone(), 
                                                    polygon_material);
                                                    
                    polygon.position.set((x * 20), (z * 20), (y * 20));
                    
                    scene.add(polygon);
                }
            }
        }
    }
    
    $('#n_texts_shown').html(MY_N_TEXTS_SHOWN);
    $('#n_boxes_shown').html(n_boxes_with_data);

}
