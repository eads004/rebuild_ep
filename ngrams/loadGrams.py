#!/home/spenteco/anaconda2/envs/py3/bin/python

import sys, os, re, csv, sqlite3

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

database = sys.argv[1]
tableName = sys.argv[2]
inputFolder = sys.argv[3]

conn = sqlite3.connect(database)
cursor = conn.cursor()

questionMarks = ''

expectedNumberOfColumns = 0
if tableName == 'unigrams':
    expectedNumberOfColumns = 8
if tableName == 'bigrams':
    expectedNumberOfColumns = 13
if tableName == 'trigrams':
    expectedNumberOfColumns = 18

N_WF_DROPPED = 0
N_WF_LOADED = 0

for fileName in os.listdir(inputFolder):
    
    inLines = open(inputFolder + fileName, 'r', encoding='utf-8').read().split('\n')

    for line in inLines:
        
        if line.strip() > '':
    
            cols = line.split('\t')
            
            if len(cols) != expectedNumberOfColumns:
                print('ERROR expectedNumberOfColumns', fileName, line, cols[-2])
                N_WF_DROPPED += int(cols[-2])
            else:

                insertData = tuple(cols[:-2] + [int(cols[-2]), int(cols[-1])])
                
                if questionMarks == '':
                    for c in cols:
                        if questionMarks > '':
                            questionMarks = questionMarks + ', '
                        questionMarks = questionMarks + '?'

                sqlStatement = 'INSERT INTO ' + tableName + ' VALUES (' + questionMarks + ')'

                cursor.execute(sqlStatement, insertData)
                
                N_WF_LOADED += int(cols[-2])
                
    conn.commit()
    
print()
print('N_WF_DROPPED', N_WF_DROPPED)
print('N_WF_LOADED', N_WF_LOADED)
