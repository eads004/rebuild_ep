#!/home/spenteco/anaconda2/envs/py3/bin/python
## -*- coding: utf-8 -*-

import sys, glob
from lxml import etree

#   --------------------------------------------------------------------
#
#   -------------------------------------------------------------------- 

if __name__ == "__main__":

    INPUT_FOLDER = sys.argv[1]
    OUTPUT_FOLDER = sys.argv[2]

    for n, path_to_file in enumerate(glob.glob(INPUT_FOLDER + '*/*.xml')):

        print('cp ' + path_to_file + ' ' + OUTPUT_FOLDER)
