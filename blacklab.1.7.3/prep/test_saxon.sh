rm temp/*
for f in /home/spenteco/0/phase1texts/texts/$1/*
do
    file_name=`basename "$f"`
    echo $f, $file_name
    java -jar /data/SaxonHE9-4-0-2J/saxon9he.jar $f prep/defaultsource.xsl > temp/$file_name
done
