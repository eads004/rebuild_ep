#!/home/spenteco/anaconda2/envs/py3/bin/python   

import sys, os, re, csv, sqlite3

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------

def dumpColumn(database, columnName, outputFolder):
    
    print('processing', columnName)

    conn = sqlite3.connect(database)

    c = conn.cursor()

    c.execute('select distinct ' + columnName + ' from unigrams order by 1')
                    
    rows = c.fetchall()

    outF = open(outputFolder + columnName + '.csv', 'w', encoding='utf-8')

    for r in rows:
        outF.write(r[0] + '\n')
        
    outF.close()

#   --------------------------------------------------------------------
#
#   --------------------------------------------------------------------
    
dumpColumn(sys.argv[1], 'spe1', sys.argv[2])
dumpColumn(sys.argv[1], 'reg1', sys.argv[2])
dumpColumn(sys.argv[1], 'lem1', sys.argv[2])
dumpColumn(sys.argv[1], 'pos1', sys.argv[2])
