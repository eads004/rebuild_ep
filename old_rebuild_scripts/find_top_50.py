#!/home/spenteco/anaconda2/envs/py2/bin/python
## coding: utf-8

import sys, glob, re, codecs, json, re
from gensim import corpora, models, similarities
from gensim.test.utils import get_tmpfile

LIKENESS_DEPTH = 'text'

LABELS_FILE_NAME = 'all_eebo.' + LIKENESS_DEPTH + '.labels.json'
INDEX_FILE_NAME = 'all_eebo.' + LIKENESS_DEPTH + '.index'
METADATA_FILE_NAME = 'EEBO_metadata.tsv'
TFIDF_CORPUS_FILE_NAME = 'all_eebo.' + LIKENESS_DEPTH + '.tfidf'
N_MATCHES_TO_REPORT = 50

def load_metadata():

    metadata = {}

    for line in codecs.open(METADATA_FILE_NAME, 'r', encoding='utf-8').read().split('\n'):
        if line.strip() > '' and len(line.strip().split('\t')) > 3:
            cols = line.strip().split('\t')
            metadata[cols[0] + '.xml'] = {'year': cols[1], 'author': cols[2], 'title': cols[3]}

    return metadata

def load_labels_json():

    file_name_labels = json.loads(codecs.open(LABELS_FILE_NAME, 'r', encoding='utf-8').read())

    return file_name_labels

def find_top_n_matches(tfidf_for_a_file, file_name_labels, index, n_to_list, metadata):

    match_to_values = []

    sims = index[tfidf_for_a_file]
    sims = sorted(enumerate(sims), key=lambda item: -item[1])

    for s in sims[1:n_to_list + 1]:
        match_to_values.append([str(s[1]),
                                file_name_labels[s[0]],
                                metadata[file_name_labels[s[0]]]['year'],
                                metadata[file_name_labels[s[0]]]['author'],
                                metadata[file_name_labels[s[0]]]['title']])

    return match_to_values

if __name__ == "__main__":
    
    metadata = load_metadata()

    file_name_labels = load_labels_json()

    corpus_tfidf = corpora.MmCorpus(TFIDF_CORPUS_FILE_NAME)

    index = similarities.Similarity.load('all_eebo.' + LIKENESS_DEPTH + '.index')

    output_keys = ['id', 'year', 'author', 'title', 
                    'match_score', 'match_id', 'match_year', 'match_author', 'match_title']

    print('\t'.join(output_keys))

    for a, tfidf_for_a_file in enumerate(corpus_tfidf):

        file_name = file_name_labels[a]

        match_from_values = [file_name,
                                metadata[file_name]['year'],
                                metadata[file_name]['author'],
                                metadata[file_name]['title']]

        matches = find_top_n_matches(tfidf_for_a_file, file_name_labels, index, N_MATCHES_TO_REPORT, metadata)

        for m in matches:
            
            print('\t'.join(match_from_values + m))
