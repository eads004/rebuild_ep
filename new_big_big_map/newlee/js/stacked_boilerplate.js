
// CAN I SHOVE THE BOILERPLATE SOMEWHERE ELSE, SO IT ISN'T IN THE WAY?

/*
	Three.js "tutorials by example"
	Author: Lee Stemkoski
	Date: July 2013 (three.js v59dev)
*/

// MAIN

// standard global variables
var container, scene, camera, renderer, controls, stats;
var keyboard = new THREEx.KeyboardState();
var clock = new THREE.Clock();

// custom global variables
var cube;
var renderer = '';

Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};


function animate()  {
    requestAnimationFrame(animate);
	render();		
	update();
}

function update() {
    
	if (keyboard.pressed("z")) { 
	}
    
    // MOUSE MOVE INTERSECTIONS
    
    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children);
    
    var which_intersect = {x: null, y: null, z: null};

    if (intersects.length > 0) {
        which_intersect = {'x': intersects[0].object.position.x,
                                    'y': intersects[0].object.position.y,
                                    'z': intersects[0].object.position.z};
    }
	
    var last_box = MY_INTERSECTED_BOXES.length - 1;
    
    if (JSON.stringify(which_intersect) === JSON.stringify(MY_INTERSECTED_BOXES[last_box])) {
    }
    else {
        MY_INTERSECTED_BOXES.push(which_intersect);
    }
                    
    if (MY_INTERSECTED_BOXES.length > 1) {
        
        var last_i = MY_INTERSECTED_BOXES[MY_INTERSECTED_BOXES.length - 1];
        
        for (var a = 0; a < scene.children.length; a++) {
            
            if (scene.children[a].type === 'Mesh') {
                
                if (scene.children[a].position.x != last_i.x || 
                    scene.children[a].position.y != last_i.y || 
                    scene.children[a].position.z != last_i.z) {
                        
                    scene.children[a].scale.set(1, 1, 1);
                }
                else {  
                    scene.children[a].scale.set(1.4, 1.4, 1.4);
                }
            }
        }
    }
    
	controls.update();
	stats.update();
}

function render()  {
	renderer.render(scene, camera);
}


