#!/usr/bin/env python
## coding: utf-8

import sys, glob, re, codecs, json, re, random
from lxml import etree

good_words = set(json.loads(codecs.open('/home/data/all_3_prototype_rebuild/good_words.json', 'r', encoding='utf-8').read()))

LIKENESS_DEPTH = 'text'

MORPHADORNER_FOLDER = '/home/data/eebo_process/adorned/'
#MORPHADORNER_FOLDER = 'test_adorned/'

INPUT_FILE_NAME = '/home/data/all_3_prototype_rebuild/all_eebo.mallet.' + LIKENESS_DEPTH + '.INPUT.txt'
OUTPUT_FILE_NAME = '/home/data/all_3_prototype_rebuild/all_eebo.mallet.' + LIKENESS_DEPTH + '.TRIMMED.INPUT.txt'

input_file = codecs.open(INPUT_FILE_NAME, 'r', encoding='utf-8')

output_file = codecs.open(OUTPUT_FILE_NAME, 'w', encoding='utf-8')

for line in input_file:
    
    if line.strip() > '':
        
        cols = re.split('\s+', line.strip())

        tokens = []
        for c in cols[2:]:
            if c in good_words:
                tokens.append(c)

        output_file.write(cols[0] + ' ' + cols[1] + ' ' + ' '.join(tokens) + '\n')

input_file.close()
output_file.close()
